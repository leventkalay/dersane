package com.ronin.middleware;

import org.primefaces.component.datascroller.DataScroller;
import org.primefaces.component.datascroller.DataScrollerRenderer;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.util.WidgetBuilder;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by fcabi on 06.06.2015.
 */
public class CustomDataScrollerRenderer extends DataScrollerRenderer {

    public CustomDataScrollerRenderer() {
    }

    public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
        DataScroller ds = (DataScroller) component;
        if (ds.isLoadRequest()) {
            String chunkSize = ds.getClientId(context);
            int offset = Integer.parseInt((String) context.getExternalContext().getRequestParameterMap().get(chunkSize + "_offset"));
            this.loadChunk(context, ds, offset, ds.getChunkSize());
        } else {
            int chunkSize1 = ds.getChunkSize();
            if (chunkSize1 == 0) {
                chunkSize1 = ds.getRowCount();
            }

            this.encodeMarkup(context, ds, chunkSize1);
            this.encodeScript(context, ds, chunkSize1);
        }

    }

    @Override
    protected void encodeMarkup(FacesContext context, DataScroller ds, int chunkSize) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String clientId = ds.getClientId(context);
        boolean inline = ds.getMode().equals("inline");
        UIComponent header = ds.getFacet("header");
        UIComponent loader = ds.getFacet("loader");
        String contentCornerClass = null;
        String containerClass = inline ? "ui-datascroller ui-datascroller-inline ui-widget" : "ui-datascroller ui-widget";
        String style = ds.getStyle();
        String userStyleClass = ds.getStyleClass();
        String styleClass = userStyleClass == null ? containerClass : containerClass + " " + userStyleClass;
        boolean alreadyLoaded = false;
        if (ds.isLazy()) {
            alreadyLoaded = true;
            loadLazyData(ds, 0, chunkSize);
        }

        writer.startElement("div", ds);
        writer.writeAttribute("id", clientId, (String) null);
        writer.writeAttribute("class", styleClass, (String) null);
        if (style != null) {
            writer.writeAttribute("style", styleClass, (String) null);
        }

        if (header != null && header.isRendered()) {
            writer.startElement("div", ds);
            writer.writeAttribute("class", "ui-datascroller-header ui-widget-header ui-corner-top", (String) null);
            header.encodeAll(context);
            writer.endElement("div");
            contentCornerClass = "ui-corner-bottom";
        } else {
            contentCornerClass = "ui-corner-all";
        }

        writer.startElement("div", ds);
        writer.writeAttribute("class", "ui-datascroller-content ui-widget-content " + contentCornerClass, (String) null);
        if (inline) {
            writer.writeAttribute("style", "height:" + ds.getScrollHeight() + "px", (String) null);
        }

        writer.startElement("ul", ds);
        writer.writeAttribute("class", "ui-datascroller-list", (String) null);
        loadChunk(context, ds, 0, chunkSize, alreadyLoaded);
        ds.setRowIndex(-1);
        writer.endElement("ul");
        writer.startElement("div", (UIComponent) null);
        writer.writeAttribute("class", "ui-datascroller-loader", (String) null);
        if (loader != null && loader.isRendered()) {
            loader.encodeAll(context);
        }

        writer.endElement("div");
        writer.endElement("div");
        writer.endElement("div");
    }

    protected void encodeScript(FacesContext context, DataScroller ds, int chunkSize) throws IOException {
        String clientId = ds.getClientId(context);
        String loadEvent = ds.getFacet("loader") == null ? "scroll" : "manual";
        WidgetBuilder wb = this.getWidgetBuilder(context);
        wb.init("DataScroller", ds.resolveWidgetVar(), clientId).attr("chunkSize", Integer.valueOf(chunkSize)).attr("totalSize", Integer.valueOf(ds.getRowCount())).attr("loadEvent", loadEvent).attr("mode", ds.getMode(), "document").attr("buffer", Integer.valueOf(ds.getBuffer())).finish();
    }

    @Override
    protected void loadChunk(FacesContext context, DataScroller ds, int start, int size) throws IOException {
        loadChunk(context, ds, start, size, false);
    }

    protected void loadChunk(FacesContext context, DataScroller ds, int start, int size, boolean alreadyLoaded) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        if (ds.isLazy() && !alreadyLoaded) {
            loadLazyData(ds, start, size);
        }

        for (int i = start; i < start + size; ++i) {
            ds.setRowIndex(i);
            if (!ds.isRowAvailable()) {
                break;
            }

            writer.startElement("li", (UIComponent) null);
            writer.writeAttribute("class", "ui-datascroller-item", (String) null);
            this.renderChildren(context, ds);
            writer.endElement("li");
        }

        ds.setRowIndex(-1);
    }

    protected void loadLazyData(DataScroller ds, int start, int size) {
        LazyDataModel lazyModel = (LazyDataModel) ds.getValue();
        if (lazyModel != null) {
            List data = lazyModel.load(start, size, (String) null, (SortOrder) null, (Map) null);
            lazyModel.setPageSize(size);
            lazyModel.setWrappedData(data);
        }

    }

    public void encodeChildren(FacesContext context, UIComponent component) throws IOException {
    }

    public boolean getRendersChildren() {
        return true;
    }
}
