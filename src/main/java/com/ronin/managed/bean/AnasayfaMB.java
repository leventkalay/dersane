package com.ronin.managed.bean;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.commmon.beans.util.JsfUtil;
import com.ronin.common.service.IKullaniciService;
import com.ronin.common.service.IOrtakService;
import com.ronin.managed.bean.lazydatamodel.LazyBelgeDataModel;
import com.ronin.model.Interfaces.IAbstractEntity;
import com.ronin.model.constant.Belge;
import com.ronin.model.constant.IletisimBilgileri;
import com.ronin.service.IFileUploadService;
import com.ronin.utils.DateUtils;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.extensions.component.gchart.model.GChartModel;
import org.primefaces.extensions.component.gchart.model.GChartType;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.chart.PieChartModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

@ManagedBean(name = "anasayfaMB")
@ViewScoped
public class AnasayfaMB implements Serializable {

    public static Logger logger = Logger.getLogger(AnasayfaMB.class);

    @ManagedProperty("#{msg}")
    private ResourceBundle message;

    @ManagedProperty("#{lbl}")
    private ResourceBundle label;

    @ManagedProperty("#{kullaniciService}")
    private IKullaniciService kullaniciService;

    @ManagedProperty("#{sessionInfo}")
    private SessionInfo sessionInfo;

    @ManagedProperty("#{fileUploadService}")
    private IFileUploadService fileUploadService;

    @ManagedProperty("#{ortakService}")
    private IOrtakService ortakService;

    private String eskiSifre;

    private String yeniSifre;

    private Belge selectedBelge;

    private LazyDataModel<Belge> belgeDataModel;

    private IletisimBilgileri yeniIletisimBilgisi = new IletisimBilgileri();
    private IletisimBilgileri selectedIletisim;

    private List<IletisimBilgileri> iletisimBilgiList;

    private DefaultStreamedContent download;

    private Belge yeniBelge = new Belge();

    private GChartType chartType = GChartType.PIE;
    private GChartModel pieModel1 = null;
    private PieChartModel pieModelAnket;

    private boolean skip;

    //combolar
    private List<IAbstractEntity> belgeTipiList;
    private List<IAbstractEntity> anketAktifPasifList;

    private int onlineVisitors;
    private int todaysVisitors;
    private int totalVisitors;

    private int anketIndex;
    private Long anketToplamOyveren;

    private int duyuruIndex;

    private int belgeIndex;

    //loading booleans

    boolean duyuruLoaded = false;
    boolean ilanLoaded = false;
    boolean belgeLoaded = false;
    boolean anketLoaded = false;
    boolean iletisimLoaded = false;
    boolean finansLoaded = false;
    boolean visitorsReportLoaded = false;
    boolean randevuLoaded = false;
    boolean bekleyenSiparisLoaded = false;

    @PostConstruct
    public void init() {
        prepareDummyLoadObjects();
    }

    public void createPieModels() {
        finansLoaded = true;
    }

    public void loadVisitorsReport() {
        onlineVisitors = kullaniciService.getActiveUsersCount(sessionInfo);
        totalVisitors = ortakService.numberOfVisitors(sessionInfo, null);
        todaysVisitors = ortakService.numberOfVisitors(sessionInfo, DateUtils.getToday());
        visitorsReportLoaded = true;
    }


    public DefaultStreamedContent getDownload() {
        try {
            InputStream input = selectedBelge.getContent().getBinaryStream();
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            download = new DefaultStreamedContent(input, externalContext.getMimeType(selectedBelge.getDataName()), selectedBelge.getDataName());
            return download;
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e.toString());
        }
        return null;
    }

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public void prepDownload() throws Exception {
        InputStream input = selectedBelge.getContent().getBinaryStream();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(selectedBelge.getDataName()), selectedBelge.getDataName()));
    }

    public void belgeUpload(FileUploadEvent event) {
        if (event.getFile().equals(null)) {
            JsfUtil.addSuccessMessage("file is null");
        }
        try {
            UploadedFile uploadedFile = event.getFile();
            fileUploadService.belgeEkle(sessionInfo, uploadedFile, yeniBelge);
            JsfUtil.addSuccessMessage(message.getString("belge_ekleme_basarili"));
        } catch (Exception e) {
            JsfUtil.addSuccessMessage("error reading file " + e);
        }

    }

    public String navigateToBelgeEkle() {
        storeFlashObjects();
        return "pages/sitePaylasimIslemleri/belgeEkleme.xhtml";
    }

    public String navigateToDuyuruEkle() {
        storeFlashObjects();
        return "pages/sitePaylasimIslemleri/duyuruEkleme.xhtml";
    }

    public void storeFlashObjects() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("backPage", "/dashboard.xhtml");
    }

    public void storeFlashObjectsForDuyuru() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("backPage", "/dashboard.xhtml");
    }

    //page navigations
    public String anketGoruntule() {
        storeFlashObjectsForAnket();
        return "pages/sitePaylasimIslemleri/anketOyKullanma.xhtml";
    }

    public void storeFlashObjectsForAnket() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("backPage", "/dashboard.xhtml");
    }

    public void clearBelgeYuklemeObject() {
        yeniBelge = new Belge();
    }

    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }

    public void prepareDummyLoadObjects() {
        belgeTipiList = ortakService.getListByNamedQuery("BelgeTipi.findAll");
        anketAktifPasifList = ortakService.getListByNamedQuery("EvetHayir.findAll");
        duyuruLoaded = false;
        ilanLoaded = false;
        belgeLoaded = false;
        anketLoaded = false;
        iletisimLoaded = false;
        finansLoaded = false;
        visitorsReportLoaded = false;
        randevuLoaded = false;
        bekleyenSiparisLoaded = false;
    }


    public void getBelgeData() {
        belgeDataModel = new LazyBelgeDataModel(fileUploadService, sessionInfo);
        belgeLoaded = true;
    }

    public void prepareIletisimBilgi() {
        yeniIletisimBilgisi = new IletisimBilgileri();
        iletisimBilgiList = getOrtakService().getAllIletisimBilgileriBySirket(sessionInfo);
        iletisimLoaded = true;
    }

    public void deleteBelge() {
        try {
            getOrtakService().deleteBelge(selectedBelge);
            JsfUtil.addSuccessMessage(message.getString("belge_silme_basarili"));
            getBelgeData();
        } catch (Exception e) {
            logger.error(e.getStackTrace());
            JsfUtil.addSuccessMessage("Hata!");
        }
    }


    public void iletisimGuncelle() {
        getOrtakService().updateIletisimBilgileri(selectedIletisim);
        prepareIletisimBilgi();
        JsfUtil.addSuccessMessage(message.getString("iletisim_guncelleme_basarili"));
    }

    public void iletisimSil() {
        getOrtakService().deleteIletisimBilgisi(selectedIletisim);
        prepareIletisimBilgi();
        JsfUtil.addSuccessMessage(message.getString("iletisim_silme_basarili"));
    }

    public void iletisimBilgisiEkle() {
        getOrtakService().iletisimBilgisiEkle(sessionInfo, yeniIletisimBilgisi);
        prepareIletisimBilgi();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('iletisimBilgisiEklePopup').hide()");
        JsfUtil.addSuccessMessage(message.getString("iletisim_ekleme_basarili"));
    }

    public String kullaniciGuncelleme() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("selectedKullaniciObject", sessionInfo.getKullanici());
        storeFlashObjects();
        return "pages/sistemYonetimi/kullaniciGuncelleme.xhtml";
    }


    public String siparisSorgulamayaGit() {
        return "pages/siparisIslemleri/siparisSorgula.xhtml";
    }

    public String siparisVermeyeGit() {
        return "pages/siparisIslemleri/siparisEkleme.xhtml";
    }

    public String gunsonuVermeyeGit() {
        return "pages/gunsonuIslemleri/gunSonuGiris.xhtml";
    }

    public int progressValueFinder(Long secimSayisi) {
        if (secimSayisi == null || anketToplamOyveren == null) {
            return 0;
        }
        return (int) ((secimSayisi.floatValue() / anketToplamOyveren.floatValue()) * 100);
    }

    public String doubleFormatter(Double deger) {
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(deger);
    }

    public GChartModel getChart() {
        return pieModel1;
    }

    public Date getCurrentDate() {
        return DateUtils.getNow();
    }


    public ResourceBundle getMessage() {
        return message;
    }

    public void setMessage(ResourceBundle message) {
        this.message = message;
    }

    public IKullaniciService getKullaniciService() {
        return kullaniciService;
    }

    public void setKullaniciService(IKullaniciService kullaniciService) {
        this.kullaniciService = kullaniciService;
    }

    public String getEskiSifre() {
        return eskiSifre;
    }

    public void setEskiSifre(String eskiSifre) {
        this.eskiSifre = eskiSifre;
    }

    public String getYeniSifre() {
        return yeniSifre;
    }

    public void setYeniSifre(String yeniSifre) {
        this.yeniSifre = yeniSifre;
    }

    public SessionInfo getSessionInfo() {
        return sessionInfo;
    }

    public void setSessionInfo(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    public List<IletisimBilgileri> getIletisimBilgiList() {
        return iletisimBilgiList;
    }

    public void setIletisimBilgiList(List<IletisimBilgileri> iletisimBilgiList) {
        this.iletisimBilgiList = iletisimBilgiList;
    }

    public IFileUploadService getFileUploadService() {
        return fileUploadService;
    }

    public void setFileUploadService(IFileUploadService fileUploadService) {
        this.fileUploadService = fileUploadService;
    }

    public LazyDataModel<Belge> getBelgeDataModel() {
        return belgeDataModel;
    }

    public void setBelgeDataModel(LazyDataModel<Belge> belgeDataModel) {
        this.belgeDataModel = belgeDataModel;
    }

    public Belge getSelectedBelge() {
        return selectedBelge;
    }

    public void setSelectedBelge(Belge selectedBelge) {
        this.selectedBelge = selectedBelge;
    }

    public Belge getYeniBelge() {
        return yeniBelge;
    }

    public void setYeniBelge(Belge yeniBelge) {
        this.yeniBelge = yeniBelge;
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public ResourceBundle getLabel() {
        return label;
    }

    public void setLabel(ResourceBundle label) {
        this.label = label;
    }

    public PieChartModel getPieModelAnket() {
        return pieModelAnket;
    }

    public void setPieModelAnket(PieChartModel pieModelAnket) {
        this.pieModelAnket = pieModelAnket;
    }

    public IletisimBilgileri getYeniIletisimBilgisi() {
        return yeniIletisimBilgisi;
    }

    public void setYeniIletisimBilgisi(IletisimBilgileri yeniIletisimBilgisi) {
        this.yeniIletisimBilgisi = yeniIletisimBilgisi;
    }

    public boolean isFinansLoaded() {
        return finansLoaded;
    }

    public void setFinansLoaded(boolean finansLoaded) {
        this.finansLoaded = finansLoaded;
    }

    public boolean isVisitorsReportLoaded() {
        return visitorsReportLoaded;
    }

    public void setVisitorsReportLoaded(boolean visitorsReportLoaded) {
        this.visitorsReportLoaded = visitorsReportLoaded;
    }

    public boolean isIletisimLoaded() {
        return iletisimLoaded;
    }

    public void setIletisimLoaded(boolean iletisimLoaded) {
        this.iletisimLoaded = iletisimLoaded;
    }

    public boolean isAnketLoaded() {
        return anketLoaded;
    }

    public void setAnketLoaded(boolean anketLoaded) {
        this.anketLoaded = anketLoaded;
    }

    public boolean isBelgeLoaded() {
        return belgeLoaded;
    }

    public void setBelgeLoaded(boolean belgeLoaded) {
        this.belgeLoaded = belgeLoaded;
    }

    public boolean isIlanLoaded() {
        return ilanLoaded;
    }

    public void setIlanLoaded(boolean ilanLoaded) {
        this.ilanLoaded = ilanLoaded;
    }

    public boolean isDuyuruLoaded() {
        return duyuruLoaded;
    }

    public void setDuyuruLoaded(boolean duyuruLoaded) {
        this.duyuruLoaded = duyuruLoaded;
    }

    public List<IAbstractEntity> getAnketAktifPasifList() {
        return anketAktifPasifList;
    }

    public void setAnketAktifPasifList(List<IAbstractEntity> anketAktifPasifList) {
        this.anketAktifPasifList = anketAktifPasifList;
    }

    public List<IAbstractEntity> getBelgeTipiList() {
        return belgeTipiList;
    }

    public void setBelgeTipiList(List<IAbstractEntity> belgeTipiList) {
        this.belgeTipiList = belgeTipiList;
    }

    public IOrtakService getOrtakService() {
        return ortakService;
    }

    public void setOrtakService(IOrtakService ortakService) {
        this.ortakService = ortakService;
    }

    public int getOnlineVisitors() {
        return onlineVisitors;
    }

    public void setOnlineVisitors(int onlineVisitors) {
        this.onlineVisitors = onlineVisitors;
    }

    public int getTodaysVisitors() {
        return todaysVisitors;
    }

    public void setTodaysVisitors(int todaysVisitors) {
        this.todaysVisitors = todaysVisitors;
    }

    public int getTotalVisitors() {
        return totalVisitors;
    }

    public void setTotalVisitors(int totalVisitors) {
        this.totalVisitors = totalVisitors;
    }

    public GChartType getChartType() {
        return chartType;
    }

    public void setChartType(GChartType chartType) {
        this.chartType = chartType;
    }

    public boolean isRandevuLoaded() {
        return randevuLoaded;
    }

    public void setRandevuLoaded(boolean randevuLoaded) {
        this.randevuLoaded = randevuLoaded;
    }

    public int getAnketIndex() {
        return anketIndex;
    }

    public void setAnketIndex(int anketIndex) {
        this.anketIndex = anketIndex;
    }

    public Long getAnketToplamOyveren() {
        return anketToplamOyveren;
    }

    public void setAnketToplamOyveren(Long anketToplamOyveren) {
        this.anketToplamOyveren = anketToplamOyveren;
    }

    public IletisimBilgileri getSelectedIletisim() {
        return selectedIletisim;
    }

    public void setSelectedIletisim(IletisimBilgileri selectedIletisim) {
        this.selectedIletisim = selectedIletisim;
    }

    public int getDuyuruIndex() {
        return duyuruIndex;
    }

    public void setDuyuruIndex(int duyuruIndex) {
        this.duyuruIndex = duyuruIndex;
    }

    public int getBelgeIndex() {
        return belgeIndex;
    }

    public void setBelgeIndex(int belgeIndex) {
        this.belgeIndex = belgeIndex;
    }

    public boolean isBekleyenSiparisLoaded() {
        return bekleyenSiparisLoaded;
    }

    public void setBekleyenSiparisLoaded(boolean bekleyenSiparisLoaded) {
        this.bekleyenSiparisLoaded = bekleyenSiparisLoaded;
    }

}
