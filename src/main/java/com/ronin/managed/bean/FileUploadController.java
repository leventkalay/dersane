package com.ronin.managed.bean;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.commmon.beans.util.JsfUtil;
import com.ronin.model.VeriAktarmaIstek;
import com.ronin.model.constant.Belge;
import com.ronin.model.constant.Durum;
import com.ronin.service.IFileUploadService;
import com.ronin.utils.DateUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.*;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.*;


@ManagedBean(name = "fileUploadMB")
@ViewScoped
public class FileUploadController implements Serializable {

    public static Logger logger = Logger.getLogger(FileUploadController.class);

    @ManagedProperty("#{fileUploadService}")
    private IFileUploadService fileUploadService;

    @ManagedProperty("#{msg}")
    private ResourceBundle message;

    @ManagedProperty("#{lbl}")
    private ResourceBundle label;

    @ManagedProperty("#{sessionInfo}")
    private SessionInfo sessionInfo;

    private Belge yeniBelge = new Belge();

    private String personelAdi;

    @PostConstruct
    public void init() {

    }

    public void belgeUpload(FileUploadEvent event) {
        if (event.getFile().equals(null)) {
            JsfUtil.addSuccessMessage("file is null");
        }
        try {
            UploadedFile uploadedFile = event.getFile();
            fileUploadService.belgeEkle(sessionInfo, uploadedFile, yeniBelge);

        } catch (Exception e) {
            JsfUtil.addSuccessMessage("error reading file " + e);
        }

    }


    public void satisAktarFileUpload(FileUploadEvent event) {
        if (event.getFile().equals(null)) {
            JsfUtil.addSuccessMessage("file is null");
        }
        InputStream file = null;
        try {
            file = event.getFile().getInputstream();
        } catch (IOException e) {
            JsfUtil.addSuccessMessage("error reading file " + e);
        }

        try {

            //Create Workbook instance holding reference to .xlsx file
            // XSSFWorkbook workbook = new XSSFWorkbook(file);

            Workbook workbook = WorkbookFactory.create(file);

            //Get first/desired sheet from the workbook
            // XSSFSheet sheet = workbook.getSheetAt(0);
            Sheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            List<VeriAktarmaIstek> veriAktarmaIstekList = new ArrayList<>();
            Integer i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                try {
                    VeriAktarmaIstek aktarmaIstek = new VeriAktarmaIstek();
                    String firmaAdi = getEncodingString(row.getCell(0).getStringCellValue());
                    aktarmaIstek.setPersonelAdi(personelAdi);
                    aktarmaIstek.setFirmaAdi(firmaAdi);
                    aktarmaIstek.setMalKodu(getEncodingString(row.getCell(1).getStringCellValue()));
                    aktarmaIstek.setMalAdi(getEncodingString(row.getCell(2).getStringCellValue()));
                    aktarmaIstek.setIslemTarihi(row.getCell(3).getDateCellValue());
                    aktarmaIstek.setFaturaNo(getEncodingString(row.getCell(4).getStringCellValue()));
                    aktarmaIstek.setMiktar((int) row.getCell(5).getNumericCellValue());
                    aktarmaIstek.setBirimMaliyet(row.getCell(6).getNumericCellValue());
                    aktarmaIstek.setCikisNetBirimMaliyet(row.getCell(7).getNumericCellValue());
                    aktarmaIstek.setCikisNetToplamMaliyet(row.getCell(8).getNumericCellValue());
                    aktarmaIstek.setToplamAlisMaliyet(row.getCell(9).getNumericCellValue());
                    aktarmaIstek.setSatisKari(row.getCell(10).getNumericCellValue());
                    aktarmaIstek.setDurum(Durum.getAktifObject());
                    aktarmaIstek.setAktarimTipi(1);
                    aktarmaIstek.setSirket(sessionInfo.getSirket());
                    fileUploadService.aktarimVerisiEkle(aktarmaIstek);
                    System.out.println("Eklendi " + i++);
                } catch (Exception e) {
                    System.out.println("Hata " + i++);
                }
            }
            file.close();
        } catch (Exception e) {
            System.out.println(e);
            JsfUtil.addSuccessMessage("excel den okuren hata meydana geldi " + e);
        }
    }


    public void urunAktarFileUpload(FileUploadEvent event) {
        if (event.getFile().equals(null)) {
            JsfUtil.addSuccessMessage("file is null");
        }
        InputStream file = null;
        try {
            file = event.getFile().getInputstream();
        } catch (IOException e) {
            JsfUtil.addSuccessMessage("error reading file " + e);
        }

        try {

            //Create Workbook instance holding reference to .xlsx file
            // XSSFWorkbook workbook = new XSSFWorkbook(file);

            Workbook workbook = WorkbookFactory.create(file);

            //Get first/desired sheet from the workbook
            // XSSFSheet sheet = workbook.getSheetAt(0);
            Sheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            List<VeriAktarmaIstek> veriAktarmaIstekList = new ArrayList<>();
            Integer i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                try {
                    VeriAktarmaIstek aktarmaIstek = new VeriAktarmaIstek();
                    aktarmaIstek.setMalKodu(getEncodingString(row.getCell(0).getStringCellValue()));
                    aktarmaIstek.setMalAdi(getEncodingString(row.getCell(1).getStringCellValue()));
                    aktarmaIstek.setAktarimTipi(2);
                    aktarmaIstek.setDurum(Durum.getAktifObject());
                    aktarmaIstek.setSirket(sessionInfo.getSirket());
                    fileUploadService.aktarimVerisiEkle(aktarmaIstek);
                    System.out.println("Eklendi " + i++);
                } catch (Exception e) {
                    System.out.println("Hata " + i++);
                }
            }
            file.close();
        } catch (Exception e) {
            System.out.println(e);
            JsfUtil.addSuccessMessage("excel den okuren hata meydana geldi " + e);
        }
    }

    public void firmaAktarFileUpload(FileUploadEvent event) {
        if (event.getFile().equals(null)) {
            JsfUtil.addSuccessMessage("file is null");
        }
        InputStream file = null;
        try {
            file = event.getFile().getInputstream();
        } catch (IOException e) {
            JsfUtil.addSuccessMessage("error reading file " + e);
        }

        try {

            //Create Workbook instance holding reference to .xlsx file
            // XSSFWorkbook workbook = new XSSFWorkbook(file);

            Workbook workbook = WorkbookFactory.create(file);

            //Get first/desired sheet from the workbook
            // XSSFSheet sheet = workbook.getSheetAt(0);
            Sheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            List<VeriAktarmaIstek> veriAktarmaIstekList = new ArrayList<>();
            Integer i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                try {
                    VeriAktarmaIstek aktarmaIstek = new VeriAktarmaIstek();
                    aktarmaIstek.setFirmaKodu(getEncodingString(row.getCell(0).getStringCellValue()));
                    aktarmaIstek.setFirmaAdi(getEncodingString(row.getCell(1).getStringCellValue()));
                    aktarmaIstek.setPersonelAdi(getEncodingString(row.getCell(2).getStringCellValue()));
                    aktarmaIstek.setAktarimTipi(3);
                    aktarmaIstek.setDurum(Durum.getAktifObject());
                    aktarmaIstek.setSirket(sessionInfo.getSirket());
                    fileUploadService.aktarimVerisiEkle(aktarmaIstek);
                    System.out.println("Eklendi " + i++);
                } catch (Exception e) {
                    System.out.println("Hata " + i++);
                }
            }
            file.close();
        } catch (Exception e) {
            System.out.println(e);
            JsfUtil.addSuccessMessage("excel den okuren hata meydana geldi " + e);
        }
    }

    public String getEncodingString(String data) {
        return data;
    }


    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        FileUploadController.logger = logger;
    }

    public IFileUploadService getFileUploadService() {
        return fileUploadService;
    }

    public void setFileUploadService(IFileUploadService fileUploadService) {
        this.fileUploadService = fileUploadService;
    }

    public ResourceBundle getMessage() {
        return message;
    }

    public void setMessage(ResourceBundle message) {
        this.message = message;
    }

    public ResourceBundle getLabel() {
        return label;
    }

    public void setLabel(ResourceBundle label) {
        this.label = label;
    }

    public SessionInfo getSessionInfo() {
        return sessionInfo;
    }

    public void setSessionInfo(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    public Belge getYeniBelge() {
        return yeniBelge;
    }

    public void setYeniBelge(Belge yeniBelge) {
        this.yeniBelge = yeniBelge;
    }

    public String getPersonelAdi() {
        return personelAdi;
    }

    public void setPersonelAdi(String personelAdi) {
        this.personelAdi = personelAdi;
    }
}
