package com.ronin.managed.bean;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.commmon.beans.util.JsfUtil;
import com.ronin.common.model.Kullanici;
import com.ronin.common.model.Rol;
import com.ronin.common.service.IOrtakService;
import com.ronin.managed.bean.lazydatamodel.BildirimDataModel;
import com.ronin.managed.bean.lazydatamodel.BildirimTipiDataModel;
import com.ronin.model.Interfaces.IAbstractEntity;
import com.ronin.model.MailSender;
import com.ronin.model.constant.BildirimTipi;
import com.ronin.model.kriter.BildirimSorguKriteri;
import com.ronin.model.kriter.BildirimTipiSorguKriteri;
import com.ronin.service.IBildirimService;
import com.ronin.utils.FlushUtils;
import org.apache.log4j.Logger;
import org.primefaces.model.DualListModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.*;

@ManagedBean(name = "bildirimMB")
@ViewScoped
public class BildirimMB extends AbstractMB  implements Serializable {

    @ManagedProperty("#{sessionInfo}")
    private SessionInfo sessionInfo;

    @ManagedProperty("#{ortakService}")
    private IOrtakService ortakService;

    @ManagedProperty("#{msg}")
    private ResourceBundle message;

    @ManagedProperty("#{lbl}")
    private ResourceBundle label;

    @ManagedProperty("#{bildirimService}")
    private IBildirimService bildirimService;


    private BildirimTipiSorguKriteri sorguKriteri = new BildirimTipiSorguKriteri();
    private BildirimSorguKriteri bildirimSorguKriteri = new BildirimSorguKriteri();

    private BildirimTipi selectedBildirimTipi;
    private MailSender selectedBildirim;

    private BildirimTipiDataModel bildirimTipiDataModel;
    private BildirimDataModel bildirimDataModel;

    private List<IAbstractEntity> bildirimTipiList;

    private DualListModel<Rol> selectedRolList;
    private List<IAbstractEntity> durumList;
    List<Rol> sourceRol = new ArrayList<Rol>();
    List<Rol> targetRol = new ArrayList<Rol>();

    @PostConstruct
    public void init() {
        getFlushObjects();
        durumList = ortakService.getListByNamedQuery("Durum.findAll");
        selectedRolList =  new DualListModel<Rol>(sourceRol, targetRol);
        bildirimTipiList = ortakService.getListByNamedQuery("BildirimTipi.findAll");
    }

    public void getFlushObjects() {
        BildirimTipiSorguKriteri sk = (BildirimTipiSorguKriteri) FlushUtils.getFlashObject("sorguKriteri");
        if (sk != null) {
            sorguKriteri = sk;
            getBildirimTipiListBySorguKriteri();
        }
    }

    public String bildirimTipiRolGuncelleme(BildirimTipi selectedBildirimTipi) {
        setSelectedBildirimTipi(selectedBildirimTipi);
        storeFlashObjects();
        return "bildirimTipiRolIliskilendirme.xhtml";
    }

    public void iptalEt(MailSender selectedMailSender) {
        bildirimService.updateMailSenderPasif(selectedMailSender);
        JsfUtil.addSuccessMessage(message.getString("msg_bildirim_gonderme_iptal_edildi"));
        getBildirimListBySorguKriteri();
    }

    public void tekrarGonder(MailSender selectedMailSender) {
        bildirimService.updateMailSenderAktif(selectedMailSender);
        JsfUtil.addSuccessMessage(message.getString("msg_bildirim_gonderme_tekrar_edildi"));
        getBildirimListBySorguKriteri();
    }

    public void storeFlashObjects() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("selectedBildirimTipiObject", selectedBildirimTipi);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("bildirimTipiSorguKriteri", sorguKriteri);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("backPage", "bildirimTipiSorgula.xhtml");
    }

    public void getBildirimTipiListBySorguKriteri() {
        List<BildirimTipi> dataList = bildirimService.getListCriteriaForPaging(0, 100, sorguKriteri , sessionInfo);
        bildirimTipiDataModel = new BildirimTipiDataModel(dataList);
    }

    public void getBildirimListBySorguKriteri() {
        List<MailSender> dataList = bildirimService.getListCriteriaForPaging(0, 100, bildirimSorguKriteri , sessionInfo);
        bildirimDataModel = new BildirimDataModel(dataList);
    }

    public List<Kullanici> completePlayer(String query) {
        List<Kullanici> suggestions = ortakService.getKullaniciByName(query, sessionInfo);
        return suggestions;
    }


    public List<IAbstractEntity> getBildirimTipiList() {
        return bildirimTipiList;
    }

    public void setBildirimTipiList(List<IAbstractEntity> bildirimTipiList) {
        this.bildirimTipiList = bildirimTipiList;
    }

    public BildirimTipiDataModel getBildirimTipiDataModel() {
        return bildirimTipiDataModel;
    }

    public void setBildirimTipiDataModel(BildirimTipiDataModel bildirimTipiDataModel) {
        this.bildirimTipiDataModel = bildirimTipiDataModel;
    }

    public BildirimTipi getSelectedBildirimTipi() {
        return selectedBildirimTipi;
    }

    public void setSelectedBildirimTipi(BildirimTipi selectedBildirimTipi) {
        this.selectedBildirimTipi = selectedBildirimTipi;
    }

    public BildirimTipiSorguKriteri getSorguKriteri() {
        return sorguKriteri;
    }

    public void setSorguKriteri(BildirimTipiSorguKriteri sorguKriteri) {
        this.sorguKriteri = sorguKriteri;
    }

    public IBildirimService getBildirimService() {
        return bildirimService;
    }

    public void setBildirimService(IBildirimService bildirimService) {
        this.bildirimService = bildirimService;
    }

    public ResourceBundle getLabel() {
        return label;
    }

    public void setLabel(ResourceBundle label) {
        this.label = label;
    }

    public ResourceBundle getMessage() {
        return message;
    }

    public void setMessage(ResourceBundle message) {
        this.message = message;
    }

    public IOrtakService getOrtakService() {
        return ortakService;
    }

    public void setOrtakService(IOrtakService ortakService) {
        this.ortakService = ortakService;
    }

    public SessionInfo getSessionInfo() {
        return sessionInfo;
    }

    public void setSessionInfo(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    public List<Rol> getTargetRol() {
        return targetRol;
    }

    public void setTargetRol(List<Rol> targetRol) {
        this.targetRol = targetRol;
    }

    public List<Rol> getSourceRol() {
        return sourceRol;
    }

    public void setSourceRol(List<Rol> sourceRol) {
        this.sourceRol = sourceRol;
    }

    public DualListModel<Rol> getSelectedRolList() {
        return selectedRolList;
    }

    public void setSelectedRolList(DualListModel<Rol> selectedRolList) {
        this.selectedRolList = selectedRolList;
    }

    public List<IAbstractEntity> getDurumList() {
        return durumList;
    }

    public void setDurumList(List<IAbstractEntity> durumList) {
        this.durumList = durumList;
    }

    public BildirimSorguKriteri getBildirimSorguKriteri() {
        return bildirimSorguKriteri;
    }

    public void setBildirimSorguKriteri(BildirimSorguKriteri bildirimSorguKriteri) {
        this.bildirimSorguKriteri = bildirimSorguKriteri;
    }

    public MailSender getSelectedBildirim() {
        return selectedBildirim;
    }

    public void setSelectedBildirim(MailSender selectedBildirim) {
        this.selectedBildirim = selectedBildirim;
    }

    public BildirimDataModel getBildirimDataModel() {
        return bildirimDataModel;
    }

    public void setBildirimDataModel(BildirimDataModel bildirimDataModel) {
        this.bildirimDataModel = bildirimDataModel;
    }
}
