package com.ronin.managed.bean;

import org.apache.log4j.Logger;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

@ManagedBean(name = "hakkimizdaMB")
@ViewScoped
public class HakkimizdaMB implements Serializable {

    public static Logger logger = Logger.getLogger(HakkimizdaMB.class);

    private MapModel advancedModel;

    private Marker marker;

    @PostConstruct
    public void init() {
        advancedModel = new DefaultMapModel();

        //Shared coordinates
        LatLng coord1 = new LatLng(39.943054,32.851069999999936);

        //Icons and Data
        advancedModel.addOverlay(new Marker(coord1,"Aymer Bilisim","aymer_logo_new.png","http://maps.google.com/mapfiles/ms/micons/blue-dot.png"));
    }

    public MapModel getAdvancedModel() {
        return advancedModel;
    }

    public void onMarkerSelect(OverlaySelectEvent event) {
        marker = (Marker) event.getOverlay();
    }

    public Marker getMarker() {
        return marker;
    }
}