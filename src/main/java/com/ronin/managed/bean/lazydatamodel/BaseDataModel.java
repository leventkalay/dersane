package com.ronin.managed.bean.lazydatamodel;


import com.ronin.model.Interfaces.IBaseEntity;
import org.primefaces.model.SelectableDataModel;

import javax.faces.model.ListDataModel;
import java.util.List;

public class BaseDataModel  extends ListDataModel<IBaseEntity> implements SelectableDataModel<IBaseEntity> {

    public BaseDataModel(List<IBaseEntity> data) {
        super(data);
    }

    @Override
    public Object getRowKey(IBaseEntity object) {
        return object.getId().toString();
    }

    @Override
    public IBaseEntity getRowData(String rowKey) {
        if(getWrappedData() == null)
            return null;
        for(IBaseEntity object : (List<IBaseEntity>)getWrappedData()) {
            if(object.getId().toString().equals(rowKey))
                return object;
        }
        return null;
    }

    public void delete(IBaseEntity object) {
        List<IBaseEntity> objectList = (List<IBaseEntity>) getWrappedData();
        objectList.remove(object);
        setWrappedData(objectList);
    }

}
