/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.managed.bean.lazydatamodel;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.model.constant.Belge;
import com.ronin.service.IFileUploadService;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LazyBelgeDataModel extends LazyDataModel<Belge> implements Serializable {

    private Long findAllCount;

    private final List<Belge> data;

    private int rowIndex;

    private SessionInfo sessionInfo;

    private IFileUploadService fileUploadService;

    public LazyBelgeDataModel(IFileUploadService fileUploadService, SessionInfo sessionInfo) {
        super();
        data = new ArrayList<>();
        this.sessionInfo = sessionInfo;
        this.fileUploadService = fileUploadService;
    }

    @Override
    public List<Belge> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        List<Belge> retData;

        if (findAllCount == null) {
            findAllCount = fileUploadService.getAdminBelgeListCount(sessionInfo);
            this.setRowCount(findAllCount.intValue());
        }

        if (first >= data.size()) {
            retData = fileUploadService.getAdminBelgeList(first, pageSize, sessionInfo);
            data.addAll(retData);
            return retData;
        } else {
            return data.subList(first, Math.min(first + pageSize, data.size()));
        }
    }

    @Override
    public void setRowIndex(int index) {
        if (index >= data.size()) {
            index = -1;
        }
        this.rowIndex = index;
    }

    @Override
    public Belge getRowData() {
        return data.get(rowIndex);
    }

    @Override
    public boolean isRowAvailable() {
        if (data == null) {
            return false;
        }
        return rowIndex >= 0 && rowIndex < data.size();
    }

}
