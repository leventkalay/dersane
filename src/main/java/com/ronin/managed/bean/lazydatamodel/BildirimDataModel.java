/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.managed.bean.lazydatamodel;

import com.ronin.model.MailSender;
import org.primefaces.model.SelectableDataModel;

import javax.faces.model.ListDataModel;
import java.util.List;

/**
 *
 * @author ealtun
 */
public class BildirimDataModel extends ListDataModel<MailSender> implements SelectableDataModel<MailSender> {

    public BildirimDataModel(List<MailSender> data) {
      super(data);
    }


    @Override
    public Object getRowKey(MailSender data) {
        return data.getId().toString();
    }


    @Override
    public MailSender getRowData(String rowKey) {
        if(getWrappedData() == null)
            return null;
       for(MailSender data : (List<MailSender>)getWrappedData()) {
           if(data.getId().toString().equals(rowKey))
           return data;
       }
       return null;
    }

}
