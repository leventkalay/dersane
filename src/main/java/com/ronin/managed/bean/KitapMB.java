package com.ronin.managed.bean;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.commmon.beans.util.JsfUtil;
import com.ronin.common.service.IKitapService;
import com.ronin.common.service.IOrtakService;
import com.ronin.managed.bean.lazydatamodel.BaseDataModel;
import com.ronin.model.Interfaces.IAbstractEntity;
import com.ronin.model.Interfaces.IBaseEntity;
import com.ronin.model.Kitap;
import com.ronin.model.constant.BildirimTipi;
import com.ronin.model.constant.BilgilendirmeTipi;
import com.ronin.model.kriter.KitapSorguKriteri;
import com.ronin.service.IBildirimService;
import com.ronin.utils.FlushUtils;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;

@ManagedBean(name = "kitapController")
@ViewScoped
public class KitapMB extends AbstractMB implements Serializable {

    public static Logger logger = Logger.getLogger(KitapMB.class);

    @ManagedProperty("#{ortakService}")
    private IOrtakService ortakService;

    @ManagedProperty("#{kitapService}")
    private IKitapService kitapService;

    @ManagedProperty("#{msg}")
    private ResourceBundle message;

    @ManagedProperty("#{lbl}")
    private ResourceBundle label;

    @ManagedProperty("#{sessionInfo}")
    private SessionInfo sessionInfo;

    private KitapSorguKriteri sorguKriteri = new KitapSorguKriteri();
    private Kitap selected;
    private Kitap yeniKitap = new Kitap();
    private BaseDataModel dataModel;


    private List<IAbstractEntity> durumList;
    private List<IAbstractEntity> dilList;

    @PostConstruct
    public void init() {
        getFlushObjects();
        prepareCombos();
    }

    public void getFlushObjects() {
        KitapSorguKriteri sk = (KitapSorguKriteri) FlushUtils.getFlashObject("sorguKriteri");
        if (sk != null) {
            sorguKriteri = sk;
            getKitapListBySorguKriteri();
        }
    }

    public String geriDon() {
        storeFlashObjects();
        return getBackPage();
    }

    public void storeFlashObjects() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("selectedKitapObject", selected);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("sorguKriteri", sorguKriteri);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("backPage", "kitapSorgula.xhtml");
    }


    public void prepareCombos() {
        durumList = ortakService.getListByNamedQuery("Durum.findAll");
        dilList = ortakService.getListByNamedQuery("Dil.findAll");
    }


    public void getKitapListBySorguKriteri() {
        try {
            List<IBaseEntity> dataList = kitapService.getListCriteriaForPaging(0, 500, sorguKriteri, sessionInfo);
            dataModel = new BaseDataModel(dataList);

        } catch (Exception e) {
            logger.error(e.getStackTrace());
            JsfUtil.addErrorMessage(e.toString());
        }
    }

    public void add() {
        kitapService.addKitap(yeniKitap, sessionInfo);
        JsfUtil.addSuccessMessage(message.getString("kullanici_ekleme_basarili"));
    }

    public void guncelle() {
        kitapService.updateKitap(selected, sessionInfo);
        JsfUtil.addSuccessMessage(message.getString("kullanici_ekleme_basarili"));
    }

    public void storeSelectedKitap(Kitap kitap) {
        setSelected(kitap);
    }

    public String update(Kitap selectedKitap) {
        return "kitapEkleme.xhtml";
    }

    public void delete(Kitap selectedKitap) {
        try {
            kitapService.kitapSil(selectedKitap);
            dataModel.delete(selectedKitap);
            JsfUtil.addSuccessMessage(message.getString("kitap_silme_basarili"));
        } catch (Exception e) {
            logger.error(e.getStackTrace());
            JsfUtil.addSuccessMessage(e.toString());
        }
    }

    public void kitapGoruntule()
    {

    }


    public IOrtakService getOrtakService() {
        return ortakService;
    }

    public void setOrtakService(IOrtakService ortakService) {
        this.ortakService = ortakService;
    }

    public IKitapService getKitapService() {
        return kitapService;
    }

    public void setKitapService(IKitapService kitapService) {
        this.kitapService = kitapService;
    }

    public ResourceBundle getMessage() {
        return message;
    }

    public void setMessage(ResourceBundle message) {
        this.message = message;
    }

    public ResourceBundle getLabel() {
        return label;
    }

    public void setLabel(ResourceBundle label) {
        this.label = label;
    }

    public SessionInfo getSessionInfo() {
        return sessionInfo;
    }

    public void setSessionInfo(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    public KitapSorguKriteri getSorguKriteri() {
        return sorguKriteri;
    }

    public void setSorguKriteri(KitapSorguKriteri sorguKriteri) {
        this.sorguKriteri = sorguKriteri;
    }

    @Override
    public Kitap getSelected() {
        return selected;
    }

    public void setSelected(Kitap selected) {
        this.selected = selected;
    }

    public Kitap getYeniKitap() {
        return yeniKitap;
    }

    public void setYeniKitap(Kitap yeniKitap) {
        this.yeniKitap = yeniKitap;
    }

    public BaseDataModel getDataModel() {
        return dataModel;
    }

    public void setDataModel(BaseDataModel dataModel) {
        this.dataModel = dataModel;
    }

    public List<IAbstractEntity> getDurumList() {
        return durumList;
    }

    public void setDurumList(List<IAbstractEntity> durumList) {
        this.durumList = durumList;
    }

    public List<IAbstractEntity> getDilList() {
        return dilList;
    }

    public void setDilList(List<IAbstractEntity> dilList) {
        this.dilList = dilList;
    }

}
