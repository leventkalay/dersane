package com.ronin.managed.bean;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.service.IOrtakService;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.ResourceBundle;

@ManagedBean(name = "mailIslemleriMB")
@ViewScoped
public class MailIslemleriMB implements Serializable {

    public static Logger logger = Logger.getLogger(MailIslemleriMB.class);
    //servisler
    @ManagedProperty("#{msg}")
    private ResourceBundle message;

    @ManagedProperty("#{lbl}")
    private ResourceBundle label;

    @ManagedProperty("#{ortakService}")
    private IOrtakService ortakService;

    @ManagedProperty("#{sessionInfo}")
    private SessionInfo sessionInfo;

    private String text;


    @PostConstruct
    public void init() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ResourceBundle getMessage() {
        return message;
    }

    public void setMessage(ResourceBundle message) {
        this.message = message;
    }

    public ResourceBundle getLabel() {
        return label;
    }

    public void setLabel(ResourceBundle label) {
        this.label = label;
    }

    public IOrtakService getOrtakService() {
        return ortakService;
    }

    public void setOrtakService(IOrtakService ortakService) {
        this.ortakService = ortakService;
    }

    public SessionInfo getSessionInfo() {
        return sessionInfo;
    }

    public void setSessionInfo(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    public void buttonAction(String deger) {
        if (StringUtils.isEmpty(text)) {
            text = deger;
        } else {
            text = text.concat(deger);
        }
    }

    public void clearText() {
        text = null;
    }
}
