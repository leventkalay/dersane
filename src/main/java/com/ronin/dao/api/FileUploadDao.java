/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.dao.api;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.model.VeriAktarmaIstek;
import com.ronin.model.constant.Belge;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * @author esimsek
 */
@Repository
public class FileUploadDao implements IFileUploadDao {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void belgeEkle(SessionInfo sessionInfo, UploadedFile uploadedFile, Belge belge) {
        try {
            saveFile(sessionInfo, uploadedFile, belge);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private Belge saveFile(SessionInfo sessionInfo, UploadedFile uploadedFile, Belge belge) throws FileNotFoundException {
        InputStream inputStream = new FileInputStream("C:\\rmsFileUploadDocument\\" + uploadedFile.getFileName());
        belge.setContent(Hibernate.getLobCreator(getSessionFactory().getCurrentSession()).createBlob(inputStream, uploadedFile.getSize()));
        belge.setDataName(uploadedFile.getFileName());
        belge.setKullanici(sessionInfo.getKullanici());
        belge.setSize(uploadedFile.getSize());
        belge.setIslemTarihi(new Date());
        belge.setSirket(sessionInfo.getSirket());
        getSessionFactory().getCurrentSession().save(belge);
        return belge;
    }


    public List<Belge> getAdminBelgeList(int first, int pageSize, SessionInfo sessionInfo) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from Belge b where not exists (select db from FirmaBelge db join db.belge bb where bb.id = b.id and bb.sirket.id = ?) and b.sirket.id =? order by b.id desc")
                .setParameter(0, sessionInfo.getSirket().getId())
                .setParameter(1, sessionInfo.getSirket().getId())
                .setFirstResult(first)
                .setMaxResults(pageSize)
                .list();
        return (List<Belge>) list;
    }


    @Override
    public Long getAdminBelgeListCount(SessionInfo sessionInfo) {
        Query query = getSessionFactory().getCurrentSession().createQuery(
                "select count(*) from Belge b where not exists (select db from FirmaBelge db join db.belge bb where bb.id = b.id and bb.sirket.id = ?) and b.sirket.id =? order by b.id desc");
        query.setLong(0, sessionInfo.getSirket().getId());
        query.setLong(1, sessionInfo.getSirket().getId());
        return (Long)query.uniqueResult();
    }

    public void aktarimVerisiEkle(VeriAktarmaIstek veriAktarmaIstek){
        getSessionFactory().getCurrentSession().save(veriAktarmaIstek);
    }

}
