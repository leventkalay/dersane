/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.dao.api;


import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.model.Kullanici;
import com.ronin.common.model.Rol;
import com.ronin.model.BildirimIstek;
import com.ronin.model.MailSender;
import com.ronin.model.Notification;
import com.ronin.model.constant.BildirimTipi;
import com.ronin.model.kriter.BildirimSorguKriteri;
import com.ronin.model.kriter.BildirimTipiSorguKriteri;
import com.ronin.model.kriter.HedefKitle;

import java.util.List;

/**
 * @author msevim
 */
public interface IBildirimDao {

    BildirimTipi getSingle(BildirimTipi bildirimTipi);

    BildirimTipi getSingleById(Long id);

    List<BildirimTipi> getList(BildirimTipi bildirimTipi);

    List<Rol> getRolListByBildirimTipi(BildirimTipi bildirimTipi, SessionInfo sessionInfo);

    List<BildirimTipi> getAllBildirimTipiList();

    void updateBldirimTipiRol(List<Rol> rolList, BildirimTipi bildirimTipi, SessionInfo sessionInfo);

    List<BildirimTipi> getListCriteriaForPaging(int first, int pageSize, BildirimTipiSorguKriteri sorguKriteri, SessionInfo sessionInfo);

    List<MailSender> getListCriteriaForPaging(int first, int pageSize, BildirimSorguKriteri sorguKriteri, SessionInfo sessionInfo);

    List<BildirimIstek> getIslenmemisBildirimIstek(SessionInfo sessionInfo);

    List<Kullanici> getHedefKitle(BildirimTipi bildirimTipi, SessionInfo sessionInfo);

    List<MailSender> getMailSender(SessionInfo sessionInfo);

    void addEmailIstek(MailSender mailSender);

    void addNotification(Notification notification);

    void updateBildirimIstekPasif(BildirimIstek bildirimIstek);

    void updateMailSenderPasif(MailSender mailSender);

    void addBildirimIstek(BildirimIstek bildirimIstek);

}
