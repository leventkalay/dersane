/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.dao.api;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.model.Kullanici;
import com.ronin.common.model.KullaniciRol;
import com.ronin.common.model.Rol;
import com.ronin.model.*;
import com.ronin.model.constant.BildirimTipi;
import com.ronin.model.constant.Durum;
import com.ronin.model.criteria.BildirimCriteria;
import com.ronin.model.criteria.BildirimTipiCriteria;
import com.ronin.model.kriter.BildirimSorguKriteri;
import com.ronin.model.kriter.BildirimTipiSorguKriteri;
import com.ronin.model.kriter.HedefKitle;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author msevim
 */
@Repository
public class BildirimDao implements IBildirimDao {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public BildirimTipi getSingle(BildirimTipi bildirimTipi) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BildirimTipi getSingleById(Long id) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from BildirimTipi where id=?")
                .setParameter(0, id).list();
        return (BildirimTipi) list.get(0);
    }

    public List<BildirimTipi> getAllBildirimTipiList() {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from BildirimTipi")
                .list();
        return (List<BildirimTipi>) list;
    }

    public List<BildirimTipi> getListCriteriaForPaging(int first, int pageSize, BildirimTipiSorguKriteri sorguKriteri, SessionInfo sessionInfo) {
        StringBuffer sb = null;

        BildirimTipiCriteria criteria = new BildirimTipiCriteria(sorguKriteri, sessionInfo, getSessionFactory().getCurrentSession(), first, pageSize);

        return (List<BildirimTipi>) criteria.prepareResult();
    }

    public List<MailSender> getListCriteriaForPaging(int first, int pageSize, BildirimSorguKriteri sorguKriteri, SessionInfo sessionInfo) {
        StringBuffer sb = null;

        BildirimCriteria criteria = new BildirimCriteria(sorguKriteri, sessionInfo, getSessionFactory().getCurrentSession(), first, pageSize);

        return (List<MailSender>) criteria.prepareResult();
    }

    public void addBildirimIstek(BildirimIstek bildirimIstek)
    {
        getSessionFactory().getCurrentSession().save(bildirimIstek);
    }


    public List<BildirimIstek> getIslenmemisBildirimIstek(SessionInfo sessionInfo) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from BildirimIstek bi where bi.durum.id = ?")
                .setParameter(0, Durum.getAktifObject().getId())
                .list();
        return (List<BildirimIstek>) list;
    }

    public List<Kullanici> getHedefKitle(BildirimTipi bildirimTipi, SessionInfo sessionInfo) {
        List<Kullanici> kullaniciList = new ArrayList<>();
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from BildirimTipiRol br " +
                        "join fetch br.rol r " +
                        "join fetch r.kullaniciRolList krl " +
                        "where br.bildirimTipi.id = ?")
                .setParameter(0, bildirimTipi.getId())
                .list();
        List<BildirimTipiRol> bildirimTipiRolList = list;

        for (BildirimTipiRol bildirimTipiRol : bildirimTipiRolList) {
            List<KullaniciRol> kullaniciRolList = bildirimTipiRol.getRol().getKullaniciRolList();
            for (KullaniciRol kullaniciRol : kullaniciRolList) {
                if (kullaniciRol.getKullanici().getEmail() != null && !StringUtils.isEmpty(kullaniciRol.getKullanici().getEmail()))
                    kullaniciList.add(kullaniciRol.getKullanici());
            }
        }

        Set<Kullanici> hs = new HashSet<>();
        hs.addAll(kullaniciList);
        kullaniciList.clear();
        kullaniciList.addAll(hs);

        return kullaniciList;
    }

    public List<MailSender> getMailSender(SessionInfo sessionInfo) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from MailSender ms where ms.durum.id = ?")
                .setParameter(0, Durum.getAktifObject().getId())
                .list();
        return (List<MailSender>) list;
    }

    public void addEmailIstek(MailSender mailSender) {
        getSessionFactory().getCurrentSession().save(mailSender);
    }

    public void addNotification(Notification notification) {
        getSessionFactory().getCurrentSession().save(notification);
    }

    public void updateBildirimIstekPasif(BildirimIstek bildirimIstek) {
        getSessionFactory().getCurrentSession().update(bildirimIstek);
    }

    public void updateMailSenderPasif(MailSender mailSender) {
        getSessionFactory().getCurrentSession().update(mailSender);
    }

    @Override
    public List<BildirimTipi> getList(BildirimTipi bildirimTipi) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    public List<Rol> getRolListByBildirimTipi(BildirimTipi bildirimTipi, SessionInfo sessionInfo) {

        List<Rol> rolList = new ArrayList<>();
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from BildirimTipiRol br join fetch br.rol r where br.bildirimTipi=? and r.sirket.id=?")
                .setParameter(0, bildirimTipi)
                .setParameter(1, sessionInfo.getSirket().getId()).list();
        for (BildirimTipiRol bildirimTipiRol : (List<BildirimTipiRol>) list) {
            rolList.add(bildirimTipiRol.getRol());
        }

        return rolList;

    }


    public void updateBldirimTipiRol(List<Rol> rolList, BildirimTipi bildirimTipi, SessionInfo sessionInfo) {
        //kullanicinin eski rolleri silinir
        Query query = getSessionFactory().getCurrentSession().createQuery("delete BildirimTipiRol br where br.bildirimTipi.id = :id and br.sirket = :sirket");
        query.setParameter("id", bildirimTipi.getId());
        query.setParameter("sirket", sessionInfo.getSirket());
        int result = query.executeUpdate();

        //yeni yetkiler eklenir.
        for (Rol rol : rolList) {
            addBildirimTipiRol(rol, bildirimTipi, sessionInfo);
        }
    }

    public void addBildirimTipiRol(Rol rol, BildirimTipi bildirimTipi, SessionInfo sessionInfo) {
        BildirimTipiRol bildirimTipiRol = new BildirimTipiRol();
        bildirimTipiRol.setRol(rol);
        bildirimTipiRol.setBildirimTipi(bildirimTipi);
        bildirimTipiRol.setSirket(sessionInfo.getSirket());
        getSessionFactory().getCurrentSession().save(bildirimTipiRol);
    }


}
