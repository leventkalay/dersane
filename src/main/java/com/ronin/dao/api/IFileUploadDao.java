/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.dao.api;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.model.VeriAktarmaIstek;
import com.ronin.model.constant.Belge;
import org.primefaces.model.UploadedFile;

import java.util.List;

/**
 * @author esimsek
 */
public interface IFileUploadDao {

    void belgeEkle(SessionInfo sessionInfo, UploadedFile uploadedFile, Belge belge);

    List<Belge> getAdminBelgeList(int first, int pageSize, SessionInfo sessionInfo);

    Long getAdminBelgeListCount(SessionInfo sessionInfo);

    void aktarimVerisiEkle(VeriAktarmaIstek veriAktarmaIstek);
}
