/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.service;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.dao.api.IFileUploadDao;
import com.ronin.model.VeriAktarmaIstek;
import com.ronin.model.constant.Belge;
import com.ronin.model.kriter.BelgeSorguKriteri;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author esimsek
 */
@Service(value = "fileUploadService")
@Transactional(readOnly =true, propagation = Propagation.SUPPORTS)
public class FileUploadService implements IFileUploadService {
    
    @Autowired
    IFileUploadDao iFileUploadDao;

    @Transactional(readOnly = false)
   public void belgeEkle(SessionInfo sessionInfo , UploadedFile uploadedFile , Belge belge){
        iFileUploadDao.belgeEkle(sessionInfo , uploadedFile , belge);
    }

    @Transactional(readOnly = false)
    public List<Belge> getAdminBelgeList(int first,int pageSize,SessionInfo sessionInfo){
       return iFileUploadDao.getAdminBelgeList(first,pageSize,sessionInfo);
    }

    @Transactional(readOnly = false)
    public Long getAdminBelgeListCount(SessionInfo sessionInfo) {
        return iFileUploadDao.getAdminBelgeListCount(sessionInfo);
    }

    @Transactional(readOnly = false)
    public void aktarimVerisiEkle(VeriAktarmaIstek veriAktarmaIstek){
        iFileUploadDao.aktarimVerisiEkle(veriAktarmaIstek);
    }

}
