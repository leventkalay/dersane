/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ronin.service;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.model.Kullanici;
import com.ronin.common.model.Rol;
import com.ronin.model.BildirimIstek;
import com.ronin.model.Kitap;
import com.ronin.model.MailSender;
import com.ronin.model.constant.BildirimTipi;
import com.ronin.model.constant.BilgilendirmeTipi;
import com.ronin.model.kriter.BildirimSorguKriteri;
import com.ronin.model.kriter.BildirimTipiSorguKriteri;
import com.ronin.model.kriter.HedefKitle;

import java.util.List;

/**
 * @author msevim
 */
public interface IBildirimService {

    BildirimTipi getSingle(BildirimTipi bildirimTipi);

    BildirimTipi getSingleById(Long id);

    List<BildirimTipi> getList(BildirimTipi bildirimTipi);

    List<Rol> getRolListByBildirimTipi(BildirimTipi bildirimTipi, SessionInfo sessionInfo);

    List<BildirimTipi> getListCriteriaForPaging(int first, int pageSize, BildirimTipiSorguKriteri sorguKriteri, SessionInfo sessionInfo);

    List<MailSender> getListCriteriaForPaging(int first, int pageSize, BildirimSorguKriteri sorguKriteri, SessionInfo sessionInfo);

    List<BildirimTipi> getAllBildirimTipiList();

    void updateBldirimTipiRol(List<Rol> rolList, BildirimTipi bildirimTipi, SessionInfo sessionInfo);

    List<Rol> getAllRolList(SessionInfo sessionInfo);

    List<BildirimIstek> getIslenmemisBildirimIstek(SessionInfo sessionInfo);

    List<Kullanici> getHedefKitle(BildirimTipi bildirimTipi, SessionInfo sessionInfo);

    List<MailSender> getMailSender(SessionInfo sessionInfo);

    void addEmailIstek(BildirimIstek bildirimIstek);

    void addEmailIstek(BildirimTipi bildirimTipi, Kullanici kullanici, String mesaj, String konu, Kitap kitap);

    void addNotification(BildirimIstek bildirimIstek);

    void updateBildirimIstekPasif(BildirimIstek bildirimIstek);

    void updateMailSenderPasif(MailSender mailSender);

    StringBuffer getMailMessage(MailSender mailSender);

    void updateMailSenderAktif(MailSender mailSender);

    void addBildirimIstek(BildirimTipi bildirimTipi, BilgilendirmeTipi bilgilendirmeTipi, String aciklama, String mesaj, Kullanici kullanici);

}
