/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.service;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.dao.api.IRaporDao;
import com.ronin.model.kriter.RaporSorguKriteri;
import com.ronin.model.sorguSonucu.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author esimsek
 */
@Service(value = "raporService")
@Transactional(readOnly =true, propagation = Propagation.SUPPORTS)
public class RaporService implements IRaporService {
    
    @Autowired
    IRaporDao iRaporDao;


    @Transactional(readOnly = false)
    public List<R101SorguSonucu> getR101ListCriteriaForPaging(RaporSorguKriteri sorguKriteri , SessionInfo sessionInfo) {
        return iRaporDao.getR101ListCriteriaForPaging(sorguKriteri , sessionInfo);
    }

    public IRaporDao getiRaporDao() {
        return iRaporDao;
    }

    public void setiRaporDao(IRaporDao iRaporDao) {
        this.iRaporDao = iRaporDao;
    }
}
