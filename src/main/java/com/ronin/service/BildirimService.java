/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.service;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.dao.IKitapDao;
import com.ronin.common.dao.IRolDao;
import com.ronin.common.model.Kullanici;
import com.ronin.common.model.Rol;
import com.ronin.dao.api.IBildirimDao;
import com.ronin.model.*;
import com.ronin.model.constant.BildirimTipi;
import com.ronin.model.constant.BilgilendirmeTipi;
import com.ronin.model.constant.Durum;
import com.ronin.model.helper.MailContentBuilder;
import com.ronin.model.kriter.BildirimSorguKriteri;
import com.ronin.model.kriter.BildirimTipiSorguKriteri;
import com.ronin.model.kriter.HedefKitle;
import com.ronin.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author msevim
 */
@Service(value = "bildirimService")
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class BildirimService implements IBildirimService {

    @Autowired
    IRolDao iRolDao;

    @Autowired
    IBildirimDao iBildirimDao;

    @Autowired
    IKitapDao kitapDao;

    @Transactional(readOnly = false)
    public BildirimTipi getSingle(BildirimTipi bildirimTipi) {
        return iBildirimDao.getSingle(bildirimTipi);
    }

    @Transactional(readOnly = false)
    public BildirimTipi getSingleById(Long id) {
        return iBildirimDao.getSingleById(id);
    }

    @Transactional(readOnly = false)
    public List<BildirimTipi> getList(BildirimTipi bildirimTipi) {
        return iBildirimDao.getList(bildirimTipi);
    }

    @Transactional(readOnly = false)
    public List<Rol> getRolListByBildirimTipi(BildirimTipi bildirimTipi, SessionInfo sessionInfo) {
        return iBildirimDao.getRolListByBildirimTipi(bildirimTipi, sessionInfo);
    }

    @Transactional(readOnly = false)
    public List<BildirimTipi> getListCriteriaForPaging(int first, int pageSize, BildirimTipiSorguKriteri sorguKriteri, SessionInfo sessionInfo) {
        return iBildirimDao.getListCriteriaForPaging(first, pageSize, sorguKriteri, sessionInfo);
    }

    @Transactional(readOnly = false)
    public List<MailSender> getListCriteriaForPaging(int first, int pageSize, BildirimSorguKriteri sorguKriteri, SessionInfo sessionInfo) {
        return iBildirimDao.getListCriteriaForPaging(first, pageSize, sorguKriteri, sessionInfo);
    }

    @Transactional(readOnly = false)
    public List<BildirimTipi> getAllBildirimTipiList() {
        return iBildirimDao.getAllBildirimTipiList();
    }

    @Transactional(readOnly = false)
    public void updateBldirimTipiRol(List<Rol> rolList, BildirimTipi bildirimTipi, SessionInfo sessionInfo) {
        iBildirimDao.updateBldirimTipiRol(rolList, bildirimTipi, sessionInfo);
    }

    @Transactional(readOnly = false)
    public List<Rol> getAllRolList(SessionInfo sessionInfo) {
        return iRolDao.getAllRolList(sessionInfo);
    }

    @Transactional(readOnly = false)
    public List<BildirimIstek> getIslenmemisBildirimIstek(SessionInfo sessionInfo) {
        return iBildirimDao.getIslenmemisBildirimIstek(sessionInfo);
    }

    @Transactional(readOnly = false)
    public List<Kullanici> getHedefKitle(BildirimTipi bildirimTipi , SessionInfo sessionInfo) {
       return iBildirimDao.getHedefKitle(bildirimTipi,sessionInfo);
    }

    @Transactional(readOnly = false)
    public List<MailSender> getMailSender(SessionInfo sessionInfo) {
        return iBildirimDao.getMailSender(sessionInfo);
    }

    @Transactional(readOnly = false)
    public void addEmailIstek(BildirimIstek bildirimIstek) {
        MailSender mailSender = new MailSender();
        mailSender.setDurum(Durum.getAktifObject());
        mailSender.setBildirimTipi(bildirimIstek.getBildirimTipi());
        mailSender.setIslemTarihi(bildirimIstek.getIslemTarihi());
        mailSender.setKullanici(bildirimIstek.getKullanici());
        mailSender.setMesaj(bildirimIstek.getMesaj());
        mailSender.setSirket(bildirimIstek.getSirket());
        mailSender.setSubject(bildirimIstek.getKisaAciklama());
        iBildirimDao.addEmailIstek(mailSender);
    }

    @Transactional(readOnly = false)
    public void addBildirimIstek(BildirimTipi bildirimTipi, BilgilendirmeTipi bilgilendirmeTipi, String aciklama, String mesaj, Kullanici kullanici)
    {
        BildirimIstek bildirimIstek = new BildirimIstek();
        bildirimIstek.setIslemTarihi(DateUtils.getNow());
        bildirimIstek.setKullanici(kullanici);
        bildirimIstek.setDurum(Durum.getAktifObject());
        bildirimIstek.setKisaAciklama(aciklama);
        bildirimIstek.setBildirimTipi(bildirimTipi);
        bildirimIstek.setBilgilendirmeTipi(bilgilendirmeTipi);
        getiBildirimDao().addBildirimIstek(bildirimIstek);
    }


    @Transactional(readOnly = false)
    public void addEmailIstek(BildirimTipi bildirimTipi, Kullanici kullanici, String mesaj, String konu, Kitap kitap) {
        MailSender mailSender = new MailSender();
        mailSender.setDurum(Durum.getAktifObject());
        mailSender.setBildirimTipi(bildirimTipi);
        mailSender.setIslemTarihi(DateUtils.getNow());
        mailSender.setKullanici(kullanici);
        mailSender.setMesaj(mesaj);
        mailSender.setSirket(new Sirket(Sirket.ENUM.VARSAYILAN_SIRKET));
        mailSender.setSubject(konu);
        iBildirimDao.addEmailIstek(mailSender);
        kitapDao.pasiflestir(kitap);
    }

    @Transactional(readOnly = false)
    public void addNotification(BildirimIstek bildirimIstek) {
        Notification notification = new Notification();
        notification.setSirket(bildirimIstek.getSirket());
        notification.setKullanici(bildirimIstek.getKullanici());
        notification.setBildirimTipi(bildirimIstek.getBildirimTipi());
        notification.setAciklama(bildirimIstek.getKisaAciklama());
        notification.setDurum(Durum.getAktifObject());
        notification.setNotification(bildirimIstek.getMesaj());
        notification.setTanimlanmaZamani(DateUtils.getNow());

        iBildirimDao.addNotification(notification);
    }

    @Transactional(readOnly = false)
    public void updateBildirimIstekPasif(BildirimIstek bildirimIstek) {
        bildirimIstek.setDurum(Durum.getPasifObject());
        iBildirimDao.updateBildirimIstekPasif(bildirimIstek);
    }

    @Transactional(readOnly = false)
    public void updateMailSenderPasif(MailSender mailSender) {
        mailSender.setDurum(Durum.getPasifObject());
        iBildirimDao.updateMailSenderPasif(mailSender);
    }

    public StringBuffer getMailMessage(MailSender mailSender) {
        MailContentBuilder builder = new MailContentBuilder();
        return builder.getEMailMessage(mailSender);
    }

    @Transactional(readOnly = false)
    public void updateMailSenderAktif(MailSender mailSender) {
        mailSender.setDurum(Durum.getAktifObject());
        iBildirimDao.updateMailSenderPasif(mailSender);
    }

    public IRolDao getiRolDao() {
        return iRolDao;
    }

    public void setiRolDao(IRolDao iRolDao) {
        this.iRolDao = iRolDao;
    }

    public IBildirimDao getiBildirimDao() {
        return iBildirimDao;
    }

    public void setiBildirimDao(IBildirimDao iBildirimDao) {
        this.iBildirimDao = iBildirimDao;
    }
}
