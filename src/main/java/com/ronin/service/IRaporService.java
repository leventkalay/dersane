/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ronin.service;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.model.kriter.RaporSorguKriteri;
import com.ronin.model.sorguSonucu.*;

import java.util.List;

/**
 *
 * @author ealtun
 */
public interface IRaporService {

    List<R101SorguSonucu> getR101ListCriteriaForPaging(RaporSorguKriteri sorguKriteri , SessionInfo sessionInfo);

}
