/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ronin.common.service;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.model.Il;
import com.ronin.common.model.Kullanici;
import com.ronin.common.model.Rol;
import com.ronin.model.Interfaces.IAbstractEntity;
import com.ronin.model.KullaniciSecim;
import com.ronin.model.KullaniciSirket;
import com.ronin.model.Notification;
import com.ronin.model.Sirket;
import com.ronin.model.constant.*;

import java.util.Date;
import java.util.List;

/**
 * @author ealtun
 */
public interface IOrtakService {

    IAbstractEntity getEntityByClass(Class cls, Long entityId);

    IAbstractEntity getSingleOneByNamedQuery(String namedQuery, Object... parameters);

    List<IAbstractEntity> getListByNamedQuery(String namedQuery, Object... parameters);

    List<IAbstractEntity> getListByNamedQuery(String namedQuery);

    List<IAbstractEntity> getListByNamedQueryWithSirket(String namedQuery, SessionInfo sessionInfo);

    List<IAbstractEntity> getIlceListByNamedQueryWithIl(String namedQuery, Il il);

    Durum getSingleDurumEntity(Long id);

    KullaniciSirket getSingleKullaniciSirketEntity(Long id);

    Sirket getSingleSirketEntity(Long id);

    Rol getSingleRolEntity(Long id);

    Integer numberOfSirket();

    List<Kullanici> getKullaniciByName(String name, SessionInfo sessionInfo);

    KullaniciSecim getKullaniciSecimByKey(Long id, String key);

    void deleteBelge(Belge belge);

    String getMD5String(String text);

    void bildirimIstekOlustur(SessionInfo sessionInfo, Kullanici kullanici, BildirimTipi.ENUM bildirimTipiEnum, String message, String kisaAciklama, BilgilendirmeTipi.ENUM bilgilendirmeTipiEnum);

    void createErisimLog(SessionInfo sessionInfo, Kullanici kullanici, LogTipi logTipi, String aciklama);

    Integer numberOfVisitors(SessionInfo sessionInfo, Date baslangicTarihi);

    List<Notification> getAllNotificationList(SessionInfo sessionInfo, int limit);

    void update(Object object);

    void save(Object object);

    List<IletisimBilgileri> getAllIletisimBilgileriBySirket(SessionInfo sessionInfo);

    void updateIletisimBilgileri(IletisimBilgileri iletisimBilgileri);

    void deleteIletisimBilgisi(IletisimBilgileri iletisimBilgileri);

    void iletisimBilgisiEkle(SessionInfo sessionInfo, IletisimBilgileri iletisimBilgileri);

    void createHataLog(SessionInfo sessionInfo, String message, String stachTrace);

    Long getOySayisiFromAnketKullanici(Long anketId, Long anketSecimId);
}
