/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.common.service;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.dao.IOrtakDao;
import com.ronin.common.model.Il;
import com.ronin.common.model.Kullanici;
import com.ronin.common.model.Rol;
import com.ronin.model.*;
import com.ronin.model.Interfaces.IAbstractEntity;
import com.ronin.model.constant.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service(value = "ortakService")
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class OrtakService implements IOrtakService {

    @Autowired
    IOrtakDao iOrtakDao;

    @Transactional(readOnly = false)
    public IAbstractEntity getEntityByClass(Class cls, Long entityId) {
        return iOrtakDao.getEntityByClass(cls, entityId);
    }

    @Transactional(readOnly = false)
    public IAbstractEntity getSingleOneByNamedQuery(String namedQuery, Object... parameters) {
        return iOrtakDao.getSingleOneByNamedQuery(namedQuery, parameters);
    }


    @Transactional(readOnly = false)
    public List<IAbstractEntity> getListByNamedQuery(String namedQuery, Object... parameters) {
        return iOrtakDao.getListByNamedQuery(namedQuery, parameters);
    }


    @Transactional(readOnly = false)
    public List<IAbstractEntity> getListByNamedQuery(String namedQuery) {
        return iOrtakDao.getListByNamedQuery(namedQuery);
    }

    @Transactional(readOnly = false)
    public List<IAbstractEntity> getListByNamedQueryWithSirket(String namedQuery, SessionInfo sessionInfo) {
        return iOrtakDao.getListByNamedQueryWithSirket(namedQuery, sessionInfo);
    }

    @Transactional(readOnly = false)
    public List<IAbstractEntity> getIlceListByNamedQueryWithIl(String namedQuery, Il il) {
        return iOrtakDao.getIlceListByNamedQueryWithIl(namedQuery, il);
    }

    @Transactional(readOnly = false)
    public Durum getSingleDurumEntity(Long id) {
        return iOrtakDao.getSingleDurumEntity(id);
    }

    @Transactional(readOnly = false)
    public KullaniciSirket getSingleKullaniciSirketEntity(Long id) {
        return iOrtakDao.getSingleKullaniciSirketEntity(id);
    }

    @Transactional(readOnly = false)
    public Sirket getSingleSirketEntity(Long id) {
        return iOrtakDao.getSingleSirketEntity(id);
    }

    @Transactional(readOnly = false)
    public Rol getSingleRolEntity(Long id) {
        return iOrtakDao.getSingleRolEntity(id);
    }

    @Transactional(readOnly = false)
    public Integer numberOfSirket() {
        return iOrtakDao.numberOfSirket();
    }


    @Transactional(readOnly = false)
    public List<Kullanici> getKullaniciByName(String name, SessionInfo sessionInfo) {
        List<Kullanici> kulList = new ArrayList<>();
        if (!StringUtils.isEmpty(name) && name.length() > 1)
            kulList = iOrtakDao.getKullaniciByName(name, sessionInfo);
        return kulList;
    }


    @Transactional(readOnly = false)
    public KullaniciSecim getKullaniciSecimByKey(Long id, String key) {
        return iOrtakDao.getKullaniciSecimByKey(id, key);
    }


    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void deleteBelge(Belge belge) {
        iOrtakDao.deleteBelge(belge);
    }

    public String getMD5String(String text) {
        String passwordToHash = text;
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(passwordToHash.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void bildirimIstekOlustur(SessionInfo sessionInfo, Kullanici kullanici, BildirimTipi.ENUM bildirimTipiEnum, String message, String kisaAciklama, BilgilendirmeTipi.ENUM bilgilendirmeTipiEnum) {
        BildirimTipi bildirimTipi = new BildirimTipi(bildirimTipiEnum);
        BilgilendirmeTipi bilgilendirmeTipi = new BilgilendirmeTipi(bilgilendirmeTipiEnum);
        if (kullanici == null) {
            BildirimIstek bildirimIstek = new BildirimIstek();
            bildirimIstek.setDurum(Durum.getAktifObject());
            bildirimIstek.setMesaj(message);
            bildirimIstek.setIslemTarihi(new Date());
            bildirimIstek.setBildirimTipi(bildirimTipi);
            bildirimIstek.setSirket(sessionInfo.getSirket());
            bildirimIstek.setKisaAciklama(kisaAciklama);
            bildirimIstek.setBilgilendirmeTipi(bilgilendirmeTipi);
            iOrtakDao.mailGonder(bildirimIstek);
        } else {
            BildirimIstek bildirimIstek = new BildirimIstek();
            bildirimIstek.setDurum(Durum.getAktifObject());
            bildirimIstek.setMesaj(message);
            bildirimIstek.setIslemTarihi(new Date());
            bildirimIstek.setBildirimTipi(bildirimTipi);
            bildirimIstek.setKullanici(kullanici);
            bildirimIstek.setSirket(sessionInfo.getSirket());
            bildirimIstek.setKisaAciklama(kisaAciklama);
            bildirimIstek.setBilgilendirmeTipi(bilgilendirmeTipi);
            iOrtakDao.mailGonder(bildirimIstek);
        }
    }

    @Transactional(readOnly = false)
    public void createErisimLog(SessionInfo sessionInfo, Kullanici kullanici, LogTipi logTipi, String aciklama) {
        iOrtakDao.createErisimLog(sessionInfo, kullanici, logTipi, aciklama);
    }

    @Transactional(readOnly = false)
    public Integer numberOfVisitors(SessionInfo sessionInfo, Date baslangicTarihi) {
        return iOrtakDao.numberOfVisitors(sessionInfo, baslangicTarihi);
    }

    public IOrtakDao getiOrtakDao() {
        return iOrtakDao;
    }

    public void setiOrtakDao(IOrtakDao iOrtakDao) {
        this.iOrtakDao = iOrtakDao;
    }

    @Transactional(readOnly = false)
    public List<Notification> getAllNotificationList(SessionInfo sessionInfo, int limit) {
        return iOrtakDao.getAllNotificationList(sessionInfo, limit);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void update(Object object) {
        iOrtakDao.update(object);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void save(Object object) {
        iOrtakDao.save(object);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public List<IletisimBilgileri> getAllIletisimBilgileriBySirket(SessionInfo sessionInfo) {
        return iOrtakDao.getAllIletisimBilgileriBySirket(sessionInfo);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void updateIletisimBilgileri(IletisimBilgileri iletisimBilgileri) {
        iOrtakDao.updateIletisimBilgileri(iletisimBilgileri);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void deleteIletisimBilgisi(IletisimBilgileri iletisimBilgileri) {
        iOrtakDao.deleteIletisimBilgisi(iletisimBilgileri);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void iletisimBilgisiEkle(SessionInfo sessionInfo, IletisimBilgileri iletisimBilgileri) {
        iletisimBilgileri.setSirket(sessionInfo.getSirket());
        iOrtakDao.iletisimBilgisiEkle(sessionInfo, iletisimBilgileri);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void createHataLog(SessionInfo sessionInfo, String message, String stachTrace) {
        iOrtakDao.createHataLog(sessionInfo, message, stachTrace);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public Long getOySayisiFromAnketKullanici(Long anketId, Long anketSecimId) {
        return iOrtakDao.getOySayisiFromAnketKullanici(anketId, anketSecimId);
    }

}
