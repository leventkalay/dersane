/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.common.service;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.dao.IKitapDao;
import com.ronin.dao.api.IBildirimDao;
import com.ronin.model.*;
import com.ronin.model.Interfaces.IBaseEntity;
import com.ronin.model.constant.BildirimTipi;
import com.ronin.model.constant.BilgilendirmeTipi;
import com.ronin.model.constant.Durum;
import com.ronin.model.kriter.KitapSorguKriteri;
import com.ronin.service.IBildirimService;
import com.ronin.utils.DateUtils;
import org.exolab.castor.types.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.faces.bean.ManagedProperty;
import java.util.Date;
import java.util.List;


@Service(value = "kitapService")
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class KitapService implements IKitapService {


    @Autowired
    IKitapDao kitapDao;

    @Autowired
    IBildirimDao bildirimDao;

    @Transactional(readOnly = true)
    public List<IBaseEntity> getListCriteriaForPaging(int first, int pageSize, KitapSorguKriteri sorguKriteri, SessionInfo sessionInfo) {
        return kitapDao.getListCriteriaForPaging(first, pageSize, sorguKriteri, sessionInfo);
    }


    public IKitapDao getKitapDao() {
        return kitapDao;
    }

    public void setKitapDao(IKitapDao kitapDao) {
        this.kitapDao = kitapDao;
    }

    @Transactional(readOnly = false)
    public Kitap addKitap(Kitap kitap , SessionInfo sessionInfo) {
        bildirimIstekEkle(sessionInfo);
        kitapEkleThread(sessionInfo);
        return getKitapDao().addKitap(kitap,sessionInfo);
    }

    @Transactional(readOnly = false)
    public Kitap updateKitap(Kitap kitap , SessionInfo sessionInfo) {
        return getKitapDao().updateKitap(kitap,sessionInfo);
    }

    @Transactional(readOnly = false)
    public void bildirimIstekEkle(SessionInfo sessionInfo)
    {
        BildirimIstek bildirimIstek = new BildirimIstek();
        bildirimIstek.setBilgilendirmeTipi(BilgilendirmeTipi.getEmailBilgilendirmeTipi());
        bildirimIstek.setBildirimTipi(BildirimTipi.getAktifKitapBildirimTipi());
        bildirimIstek.setKullanici(sessionInfo.getKullanici());
        bildirimIstek.setKisaAciklama("Levocan Kisa a�?klama");
        bildirimIstek.setDurum(Durum.getAktifObject());
        bildirimIstek.setMesaj("levo mesaj");
        bildirimIstek.setIslemTarihi(DateUtils.getNow());

        bildirimDao.addBildirimIstek(bildirimIstek);
    }

    @Transactional(readOnly = false)
    public void kitapSil( Kitap kitap) {
        getKitapDao().kitapSil(kitap);
    }

    @Transactional(readOnly = false)
    public List<KullaniciKitap> kullaniciAktifKitapGetir( ) {
        return getKitapDao().kullaniciAktifKitapGetir();
    }

    public List<Kitap> aktifKitapGetir() {
        return getKitapDao().aktifKitapGetir();
    }


    @Async
    public void kitapEkleThread(SessionInfo sessionInfo) {


        // toplu i?lem kayd?n? at
        topluIslemEkle(sessionInfo, "Kitap");

        // as?l i?ini yap

        // toplu i?lemi bitir
    }

    @Transactional(readOnly = false)
    public void topluIslemEkle(SessionInfo sessionInfo, String ad) {
         getKitapDao().topluIslemEkle(sessionInfo,ad);
    }
}
