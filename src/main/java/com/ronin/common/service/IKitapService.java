/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ronin.common.service;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.model.Il;
import com.ronin.common.model.Kullanici;
import com.ronin.common.model.Rol;
import com.ronin.model.*;
import com.ronin.model.Interfaces.IAbstractEntity;
import com.ronin.model.Interfaces.IBaseEntity;
import com.ronin.model.constant.*;
import com.ronin.model.kriter.KitapSorguKriteri;

import java.util.Date;
import java.util.List;

/**
 * @author ealtun
 */
public interface IKitapService {

    List<IBaseEntity> getListCriteriaForPaging(int first, int pageSize, KitapSorguKriteri sorguKriteri, SessionInfo sessionInfo);
    Kitap addKitap(Kitap kitap , SessionInfo sessionInfo);
    Kitap updateKitap(Kitap kitap , SessionInfo sessionInfo);
    void kitapSil(Kitap kitap);
    List<KullaniciKitap> kullaniciAktifKitapGetir();
    List<Kitap> aktifKitapGetir();

}
