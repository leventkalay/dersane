package com.ronin.common.service;
/**
 * @author msevim
 */

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.model.Kullanici;
import com.ronin.model.KullaniciSirket;
import com.ronin.model.SifreHatirlatma;
import com.ronin.model.kriter.KullaniciSorguKriteri;

import java.util.List;


public interface IKullaniciService {

    public Kullanici addKullanici(Kullanici user);

    public void updateKullanici(Kullanici user);

    public void updateKullaniciWithPasword(Kullanici user);

    public void deleteKullanici(Kullanici user);

    public Kullanici getKullaniciById(Long id);

    public Kullanici getKullaniciByUsername(String username);

    public Kullanici getKullaniciByUsernamePassword(String username, String password);

    public Kullanici getKullaniciByEmail(String email);

    public long getBekleyenSifreIslemi(Long kullaniciId);

    public List<Kullanici> getKullaniciList();

    public List<Kullanici> getListCriteriaForPaging(int first, int pageSize, KullaniciSorguKriteri sorguKriteri, SessionInfo sessionInfo);

    public KullaniciSirket addKullaniciSirket(KullaniciSirket kullaniciSirket);

    public void sifreHatirlatmaIstekGonder(SifreHatirlatma sifreHatirlatma);

    public int getActiveUsersCount(SessionInfo sessionInfo);
}
