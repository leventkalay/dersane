package com.ronin.common.model;

import com.ronin.model.Interfaces.IBaseEntity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public class AbstractEntity implements IBaseEntity, Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    protected Long id;

    @JoinColumn(name = "ekleyen_kullanici_id", referencedColumnName = "id")
    @ManyToOne
    protected Kullanici ekleyenKullanici;

    @Column(name = "islem_tarihi")
    protected Date islemTarihi;

    @JoinColumn(name = "guncelleyen_kullanici_id", referencedColumnName = "id")
    @ManyToOne
    protected Kullanici guncelleyenKullanici;

    @Column(name = "guncelleme_tarihi")
    protected Date guncellemeTarihi;

    @Version
    @Column(name = "versiyon")
    protected Long versiyon;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Kullanici getEkleyenKullanici() {
        return ekleyenKullanici;
    }

    public void setEkleyenKullanici(Kullanici kullanici) {
        this.ekleyenKullanici = kullanici;
    }

    public Date getIslemTarihi() {
        return islemTarihi;
    }

    public void setIslemTarihi(Date islemTarihi) {
        this.islemTarihi = islemTarihi;
    }

    public Kullanici getGuncelleyenKullanici() {
        return guncelleyenKullanici;
    }

    public void setGuncelleyenKullanici(Kullanici guncelleyenKullanici) {
        this.guncelleyenKullanici = guncelleyenKullanici;
    }

    public Date getGuncellemeTarihi() {
        return guncellemeTarihi;
    }

    public void setGuncellemeTarihi(Date guncellemeTarihi) {
        this.guncellemeTarihi = guncellemeTarihi;
    }

    public Long getVersiyon() {
        return versiyon;
    }

    public void setVersiyon(Long versiyon) {
        this.versiyon = versiyon;
    }
}