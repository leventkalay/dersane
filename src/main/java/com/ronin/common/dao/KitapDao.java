/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.common.dao;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.model.Kullanici;
import com.ronin.model.*;
import com.ronin.model.Interfaces.IBaseEntity;
import com.ronin.model.constant.Durum;
import com.ronin.model.constant.Status;
import com.ronin.model.criteria.KitapCriteria;
import com.ronin.model.kriter.KitapSorguKriteri;
import com.ronin.utils.DateUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author msevim
 */
@Repository
public class KitapDao extends BaseDao implements IKitapDao {

    public List<IBaseEntity> getListCriteriaForPaging(int first, int pageSize, KitapSorguKriteri sorguKriteri, SessionInfo sessionInfo) {
        StringBuffer sb = null;
        KitapCriteria criteria = new KitapCriteria(sorguKriteri, sessionInfo, getSessionFactory().getCurrentSession(), first, pageSize);
        return (List<IBaseEntity>) criteria.prepareResult();
    }

    public Kitap addKitap(Kitap kitap, SessionInfo sessionInfo) {
        return (Kitap) save(kitap, sessionInfo);
    }

    public Kitap updateKitap(Kitap kitap, SessionInfo sessionInfo) {
        return (Kitap) update(kitap, sessionInfo);
    }

    public void kitapSil(Kitap kitap) {
        getSessionFactory().getCurrentSession().delete(kitap);
    }

    public List<KullaniciKitap> kullaniciAktifKitapGetir(){
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from KullaniciKitap where durum_id=?")
                .setParameter(0, "durum.aktif").list();
        return (List<KullaniciKitap>) list;
    }

    public List<Kitap> aktifKitapGetir(){
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from Kitap where durum_id=?")
                .setParameter(0, 1).list();
        return (List<Kitap>) list;
    }

    public void pasiflestir(Kitap kitap)
    {  kitap.setDurum(Durum.getPasifObject());
       getSessionFactory().getCurrentSession().update(kitap);
    }

    public void topluIslemEkle(SessionInfo sessionInfo, String ad)
    {
        TopluIslem topluIslem = new TopluIslem();

        topluIslem.setAd(ad);
        topluIslem.setBaslamaTarihi(DateUtils.getNow());
        topluIslem.setBitisTarihi(DateUtils.getNow());
        topluIslem.setStatus(Status.EnumStatus.DEVAM_EDIYOR);
        save(topluIslem, sessionInfo);
    }





}
