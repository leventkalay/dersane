package com.ronin.common.dao;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.model.Kullanici;
import com.ronin.model.KullaniciSirket;
import com.ronin.model.SifreHatirlatma;
import com.ronin.model.kriter.KullaniciSorguKriteri;

import java.util.List;

public interface IKullaniciDAO {

	public Kullanici addKullanici(Kullanici user);

	public void updateKullanici(Kullanici user);

	public void deleteKullanici(Kullanici user);

	public Kullanici getKullaniciById(Long id);
        
    public Kullanici getKullaniciByUsername(String username);

    public Kullanici getKullaniciByUsernamePassword(String username,String password);

    public Kullanici getKullaniciByEmail(String email);

    public long getBekleyenSifreIslemi(Long kullaniciId);

    List<Kullanici> getListCriteriaForPaging(int first, int pageSize, KullaniciSorguKriteri sorguKriteri , SessionInfo sessionInfo);

	public List<Kullanici> getKullaniciList();

    KullaniciSirket addKullaniciSirket(KullaniciSirket kullaniciSirket);

    void sifreHatirlatmaIstekGonder(SifreHatirlatma sifreHatirlatma);
}
