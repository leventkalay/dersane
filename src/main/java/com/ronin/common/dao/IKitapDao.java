/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.common.dao;


import com.ronin.commmon.beans.SessionInfo;
import com.ronin.model.*;
import com.ronin.model.Interfaces.IBaseEntity;
import com.ronin.model.kriter.KitapSorguKriteri;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;

/**
 * @author msevim
 */
public interface IKitapDao {

    List<IBaseEntity> getListCriteriaForPaging(int first, int pageSize, KitapSorguKriteri sorguKriteri, SessionInfo sessionInfo);

    Kitap addKitap(Kitap kitap, SessionInfo sessionInfo);

    Kitap updateKitap(Kitap kitap, SessionInfo sessionInfo);

    void kitapSil(Kitap kitap);

    List<KullaniciKitap> kullaniciAktifKitapGetir();

    List<Kitap> aktifKitapGetir();

    void pasiflestir(Kitap kitap);

    void topluIslemEkle(SessionInfo sessionInfo, String ad);
}
