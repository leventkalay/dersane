/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.common.dao;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.model.Il;
import com.ronin.common.model.Kullanici;
import com.ronin.common.model.Rol;
import com.ronin.model.*;
import com.ronin.model.Interfaces.IAbstractEntity;
import com.ronin.model.constant.Belge;
import com.ronin.model.constant.Durum;
import com.ronin.model.constant.IletisimBilgileri;
import com.ronin.model.constant.LogTipi;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author msevim
 */
@Repository
public class OrtakDao implements IOrtakDao {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public IAbstractEntity getEntityByClass(Class cls, Long entityId) {
        return (IAbstractEntity) sessionFactory.getCurrentSession().get(cls, entityId);
    }

    @Override
    public IAbstractEntity getSingleOneByNamedQuery(String namedQuery, Object... parameters) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<IAbstractEntity> getListByNamedQuery(String namedQuery, Object... parameters) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<IAbstractEntity> getListByNamedQuery(String namedQuery) {
        return sessionFactory.getCurrentSession().getNamedQuery(namedQuery).list();
    }

    @Override
    public List<IAbstractEntity> getListByNamedQueryWithSirket(String namedQuery, SessionInfo sessionInfo) {
        return sessionFactory.getCurrentSession().getNamedQuery(namedQuery).setLong("sirketId", sessionInfo.getSirket().getId()).list();
    }

    public List<IAbstractEntity> getIlceListByNamedQueryWithIl(String namedQuery, Il il) {
        return sessionFactory.getCurrentSession().getNamedQuery(namedQuery).setLong("ilId", il.getId()).list();
    }

    public List<Notification> getAllNotificationList(SessionInfo sessionInfo, int limit) {
        Query query = getSessionFactory().getCurrentSession()
                .createQuery("from Notification n where n.kullanici.id = ?" +
                        "and n.sirket.id = ? " +
                        "and n.durum.id <> ? " +
                        "order by n.durum.id asc,n.id desc")
                .setParameter(0, sessionInfo.getKullanici().getId())
                .setParameter(1, sessionInfo.getSirket().getId())
                .setParameter(2, Durum.getSilinmisObject().getId());
        query.setMaxResults(limit);
        List list = query.list();
        return (List<Notification>) list;
    }

    public List<Kullanici> getKullaniciByName(String name, SessionInfo sessionInfo) {
        List<Kullanici> kullaniciList = new ArrayList<Kullanici>();
        List list = getSessionFactory().getCurrentSession()
                .createQuery("select ks from KullaniciSirket ks " +
                        "join fetch ks.kullanici k " +
                        "where ks.durum.id = ? " +
                        "and k.durum.id = ? " +
                        "and ks.sirket.id = ? " +
                        " and k.ad LIKE ? ")
                .setParameter(0, Durum.ENUM.Aktif.getId())
                .setParameter(1, Durum.ENUM.Aktif.getId())
                .setParameter(2, sessionInfo.getSirket().getId())
                .setParameter(3, "%" + name + "%")
                .list();

        for (KullaniciSirket ks : (List<KullaniciSirket>) list) {
            kullaniciList.add(ks.getKullanici());
        }

        return kullaniciList;
    }

    public KullaniciSecim getKullaniciSecimByKey(Long id, String key) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("select ks from KullaniciSecim ks " +
                        "where ks.kullaniciId = ? and ks.key = ?")
                .setParameter(0, id)
                .setParameter(1,key)
                .list();
        if(list.isEmpty()){
            return null;
        }
        return (KullaniciSecim) list.get(0);
    }

    public Durum getSingleDurumEntity(Long id) {
        return (Durum) sessionFactory.getCurrentSession().get(Durum.class, id);
    }

    public KullaniciSirket getSingleKullaniciSirketEntity(Long id) {
        return (KullaniciSirket) sessionFactory.getCurrentSession().get(KullaniciSirket.class, id);
    }

    public Sirket getSingleSirketEntity(Long id) {
        return (Sirket) sessionFactory.getCurrentSession().get(Sirket.class, id);
    }

    public Rol getSingleRolEntity(Long id) {
        return (Rol) sessionFactory.getCurrentSession().get(Rol.class, id);
    }

    public Integer numberOfSirket() {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("select count(S.id) from Sirket S").list();
        return ((Long) list.get(0)).intValue();
    }

    public void deleteBelge(Belge belge) {
        getSessionFactory().getCurrentSession().delete(belge);
    }

    public void mailGonder(Object mailSender) {
        getSessionFactory().getCurrentSession().save(mailSender);
    }

    public void createErisimLog(SessionInfo sessionInfo, Kullanici kullanici, LogTipi logTipi, String aciklama) {
        ErisimLog erisimLog = new ErisimLog();
        erisimLog.setAciklama(aciklama);
        erisimLog.setKullanici(kullanici);
        erisimLog.setLogTipi(logTipi);
        erisimLog.setTanitimZamani(new Date());
        erisimLog.setSirket(sessionInfo != null ? sessionInfo.getSirket() : null);
        getSessionFactory().getCurrentSession().save(erisimLog);
    }

    public Integer numberOfVisitors(SessionInfo sessionInfo, Date baslangicTarihi) {
        if (baslangicTarihi == null) {
            List list = getSessionFactory().getCurrentSession()
                    .createQuery("select distinct(E.kullanici.id) from ErisimLog E where E.sirket.id = ? and E.logTipi = ?")
                    .setParameter(0, sessionInfo.getSirket().getId())
                    .setParameter(1, LogTipi.getLoginObject())
                    .list();
            return list.size();
        } else {
            List list = getSessionFactory().getCurrentSession()
                    .createQuery("select distinct(E.kullanici.id) from ErisimLog E where E.sirket.id = ? and E.logTipi = ? and E.tanitimZamani >= ?")
                    .setParameter(0, sessionInfo.getSirket().getId())
                    .setParameter(1, LogTipi.getLoginObject())
                    .setParameter(2, baslangicTarihi)
                    .list();
            return list.size();
        }
    }


    public void update(Object object) {
        getSessionFactory().getCurrentSession().update(object);
    }

    public void save(Object object) {
        getSessionFactory().getCurrentSession().save(object);
    }

    public List<IletisimBilgileri> getAllIletisimBilgileriBySirket(SessionInfo sessionInfo) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("select ib from IletisimBilgileri ib " +
                        "where ib.sirket.id = ? ")
                .setParameter(0, sessionInfo.getSirket().getId())
                .list();
        return (List<IletisimBilgileri>) list;
    }

    public void updateIletisimBilgileri(IletisimBilgileri iletisimBilgileri) {
        getSessionFactory().getCurrentSession().update(iletisimBilgileri);
    }

    public void deleteIletisimBilgisi(IletisimBilgileri iletisimBilgileri) {
        getSessionFactory().getCurrentSession().delete(iletisimBilgileri);
    }

    public void iletisimBilgisiEkle(SessionInfo sessionInfo, IletisimBilgileri iletisimBilgileri) {
        getSessionFactory().getCurrentSession().save(iletisimBilgileri);
    }

    public void createHataLog(SessionInfo sessionInfo, String message, String stachTrace) {
        SistemHata sistemHata = new SistemHata();
        sistemHata.initSistemHata(sessionInfo, message, stachTrace);
        getSessionFactory().getCurrentSession().save(sistemHata);
    }

    public Long getOySayisiFromAnketKullanici(Long anketId, Long anketSecimId) {
        Query query = getSessionFactory().getCurrentSession().createQuery(
                "select count(*) from AnketKullanici ak where ak.anket.id=:anketId and ak.anketSecim.id=:anketSecimId");
        query.setLong("anketId", anketId);
        query.setLong("anketSecimId", anketSecimId);
        return  (Long) query.uniqueResult();
    }
}
