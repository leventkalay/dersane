/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.common.dao;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.model.Il;
import com.ronin.common.model.Kullanici;
import com.ronin.common.model.Rol;
import com.ronin.model.*;
import com.ronin.model.Interfaces.IAbstractEntity;
import com.ronin.model.Interfaces.IBaseEntity;
import com.ronin.model.constant.Belge;
import com.ronin.model.constant.Durum;
import com.ronin.model.constant.IletisimBilgileri;
import com.ronin.model.constant.LogTipi;
import com.ronin.utils.DateUtils;
import org.apache.poi.ss.formula.functions.T;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author msevim
 */
@Repository
public class BaseDao {
    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public IBaseEntity save(IBaseEntity object, SessionInfo sessionInfo) {
        object.setIslemTarihi(DateUtils.getNow());
        object.setEkleyenKullanici(sessionInfo.getKullanici());
        object.setGuncellemeTarihi(DateUtils.getNow());
        object.setGuncelleyenKullanici(sessionInfo.getKullanici());
        object.setVersiyon(0L);
        Long objectId = (Long) getSessionFactory().getCurrentSession().save(object);
        return (IBaseEntity) sessionFactory.getCurrentSession().get(object.getClass(), objectId);
    }

    public IBaseEntity update(IBaseEntity object, SessionInfo sessionInfo) {
        object.setGuncellemeTarihi(DateUtils.getNow());
        object.setGuncelleyenKullanici(sessionInfo.getKullanici());
        getSessionFactory().getCurrentSession().update(object);
        return (IBaseEntity) sessionFactory.getCurrentSession().get(object.getClass(), object.getId());
    }
}
