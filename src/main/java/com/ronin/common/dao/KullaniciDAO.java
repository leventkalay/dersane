package com.ronin.common.dao;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.model.Kullanici;
import com.ronin.model.KullaniciSirket;
import com.ronin.model.SifreHatirlatma;
import com.ronin.model.constant.BildirimTipi;
import com.ronin.model.constant.Durum;
import com.ronin.model.constant.EvetHayir;
import com.ronin.model.constant.KullaniciTipi;
import com.ronin.model.criteria.KullaniciCriteria;
import com.ronin.model.kriter.KullaniciSorguKriteri;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


@Repository
public class KullaniciDAO implements IKullaniciDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Kullanici addKullanici(Kullanici user) {
        Long kullaniciId = (Long) getSessionFactory().getCurrentSession().save(user);
        return (Kullanici) sessionFactory.getCurrentSession().get(Kullanici.class, kullaniciId);
    }

    public void deleteKullanici(Kullanici user) {
        getSessionFactory().getCurrentSession().delete(user);
    }

    public void updateKullanici(Kullanici user) {
        getSessionFactory().getCurrentSession().update(user);
    }

    public Kullanici getKullaniciById(Long id) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from Kullanici where id=?")
                .setParameter(0, id).list();
        return (Kullanici) list.get(0);
    }
    
    public Kullanici getKullaniciByUsername(String username) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from Kullanici where username=? and durum=?")
                .setParameter(0, username)
                .setParameter(1 , Durum.getAktifObject())
                .list();
        if(list.size()>0){
            return (Kullanici) list.get(0);
        }
        return null;
    }

    public Kullanici getKullaniciByUsernamePassword(String username,String password){
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from Kullanici where username=? and durum=? and password=?")
                .setParameter(0, username)
                .setParameter(1 , Durum.getAktifObject())
                .setParameter(2, password)
                .list();
        if(list.size()>0){
            return (Kullanici) list.get(0);
        }
        return null;
    }

    public Kullanici getKullaniciByEmail(String email) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from Kullanici where email=? and durum=?")
                .setParameter(0, email)
                .setParameter(1 , Durum.getAktifObject())
                .list();
        if(list.size()>0){
            return (Kullanici) list.get(0);
        }
        return null;
    }

    public long getBekleyenSifreIslemi(Long kullaniciId) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("select count(bi.id) from BildirimIstek bi where bi.kullanici.id=? and bi.bildirimTipi.id=? and bi.durum.id=?")
                .setParameter(0, kullaniciId)
                .setParameter(1, BildirimTipi.ENUM.PASSWORD_RESET.getId())
                .setParameter(2, Durum.getAktifObject().getId())
                .list();
        return (Long) list.get(0);
    }

    public List<Kullanici> getKullaniciList() {
        List list = getSessionFactory().getCurrentSession().createQuery("from Kullanici").setCacheable(true).list();
        return list;
    }

    public List<Kullanici> getListCriteriaForPaging(int first, int pageSize, KullaniciSorguKriteri sorguKriteri , SessionInfo sessionInfo) {

        StringBuffer sb = null;

        KullaniciCriteria criteria = new KullaniciCriteria(sorguKriteri, sessionInfo ,getSessionFactory().getCurrentSession(), first, pageSize);


        return (List<Kullanici>) criteria.prepareResult();
    }
    public KullaniciSirket addKullaniciSirket(KullaniciSirket kullaniciSirket) {
        Long kullaniciSirketId = (Long) getSessionFactory().getCurrentSession().save(kullaniciSirket);
        return (KullaniciSirket) sessionFactory.getCurrentSession().get(KullaniciSirket.class, kullaniciSirketId);
    }

    public void sifreHatirlatmaIstekGonder(SifreHatirlatma sifreHatirlatma) {
        getSessionFactory().getCurrentSession().save(sifreHatirlatma);
    }

}
