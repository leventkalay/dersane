/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.model.kriter;

import com.ronin.common.model.Kullanici;
import com.ronin.model.constant.BelgeTipi;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author msevim
 */
public class BelgeSorguKriteri implements Serializable{

    private BelgeTipi belgeTipi;
    private String belgeAdi;
    private Date sorguBaslangicTarihi;
    private Date sorguBitisTarihi;
    private Integer firmaNo;
    private Kullanici kullanici;

    public Date getSorguBitisTarihi() {
        return sorguBitisTarihi;
    }

    public void setSorguBitisTarihi(Date sorguBitisTarihi) {
        this.sorguBitisTarihi = sorguBitisTarihi;
    }

    public Date getSorguBaslangicTarihi() {
        return sorguBaslangicTarihi;
    }

    public void setSorguBaslangicTarihi(Date sorguBaslangicTarihi) {
        this.sorguBaslangicTarihi = sorguBaslangicTarihi;
    }

    public String getBelgeAdi() {
        return belgeAdi;
    }

    public void setBelgeAdi(String belgeAdi) {
        this.belgeAdi = belgeAdi;
    }

    public Integer getFirmaNo() {
        return firmaNo;
    }

    public void setFirmaNo(Integer firmaNo) {
        this.firmaNo = firmaNo;
    }

    public Kullanici getKullanici() {
        return kullanici;
    }

    public void setKullanici(Kullanici kullanici) {
        this.kullanici = kullanici;
    }

    public BelgeTipi getBelgeTipi() {
        return belgeTipi;
    }

    public void setBelgeTipi(BelgeTipi belgeTipi) {
        this.belgeTipi = belgeTipi;
    }
}
