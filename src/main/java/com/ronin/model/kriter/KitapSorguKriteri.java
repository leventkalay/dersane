/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ronin.model.kriter;

import com.ronin.common.model.Kullanici;
import com.ronin.common.model.Rol;
import com.ronin.model.constant.Dil;
import com.ronin.model.constant.Durum;
import com.ronin.model.constant.EvetHayir;

import java.io.Serializable;

/**
 *
 * @author msevim
 */
public class KitapSorguKriteri implements Serializable{

    private String ad;
    private Dil dil;
    private Durum durum;
    private Kullanici kullanici;



    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public Dil getDil() {
        return dil;
    }

    public void setDil(Dil dil) {
        this.dil = dil;
    }

    public Durum getDurum() {
        return durum;
    }

    public void setDurum(Durum durum) {
        this.durum = durum;
    }

    public Kullanici getKullanici() {
        return kullanici;
    }

    public void setKullanici(Kullanici kullanici) {
        this.kullanici = kullanici;
    }
}
