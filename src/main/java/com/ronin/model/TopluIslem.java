package com.ronin.model;

import com.ronin.common.model.AbstractEntity;
import com.ronin.model.constant.Status;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by levent on 4/1/2017.
 */
@Entity
@Table(name ="TOPLU_ISLEM" )
public class TopluIslem extends AbstractEntity {

    @Column(name = "ad", length = 3096)
    private String ad;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status.EnumStatus status;

    @Column(name = "baslama_tarihi")
    private Date baslamaTarihi;

    @Column(name = "bitis_tarihi")
    private Date bitisTarihi;


    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public Status.EnumStatus getStatus() {
        return status;
    }

    public void setStatus(Status.EnumStatus status) {
        this.status = status;
    }

    public Date getBaslamaTarihi() {
        return baslamaTarihi;
    }

    public void setBaslamaTarihi(Date baslamaTarihi) {
        this.baslamaTarihi = baslamaTarihi;
    }

    public Date getBitisTarihi() {
        return bitisTarihi;
    }

    public void setBitisTarihi(Date bitisTarihi) {
        this.bitisTarihi = bitisTarihi;
    }
}
