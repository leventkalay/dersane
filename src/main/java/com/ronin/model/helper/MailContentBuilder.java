package com.ronin.model.helper;

import com.ronin.model.MailSender;

/**
 * Created by ealtun on 19.04.2016.
 */
public class MailContentBuilder {


    private StringBuffer getCssPart() {
        StringBuffer css = new StringBuffer();
        css.append("<!doctype html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<meta name=\"viewport\" content=\"width=device-width\">\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
                "<title>Really Simple HTML Email Template</title>\n" +
                "<style>\n" +
                "/* -------------------------------------\n" +
                "    GLOBAL\n" +
                "------------------------------------- */\n" +
                "* {\n" +
                "  font-family: \"Helvetica Neue\", \"Helvetica\", Helvetica, Arial, sans-serif;\n" +
                "  font-size: 100%;\n" +
                "  line-height: 1.6em;\n" +
                "  margin: 0;\n" +
                "  padding: 0;\n" +
                "}\n" +
                "img {\n" +
                "  max-width: 600px;\n" +
                "  width: auto;\n" +
                "}\n" +
                "body {\n" +
                "  -webkit-font-smoothing: antialiased;\n" +
                "  height: 100%;\n" +
                "  -webkit-text-size-adjust: none;\n" +
                "  width: 100% !important;\n" +
                "}\n" +
                "/* -------------------------------------\n" +
                "    ELEMENTS\n" +
                "------------------------------------- */\n" +
                "a {\n" +
                "  color: #348eda;\n" +
                "}\n" +
                ".btn-primary {\n" +
                "  Margin-bottom: 10px;\n" +
                "  width: auto !important;\n" +
                "}\n" +
                ".btn-primary td {\n" +
                "  background-color: #348eda; \n" +
                "  border-radius: 25px;\n" +
                "  font-family: \"Helvetica Neue\", Helvetica, Arial, \"Lucida Grande\", sans-serif; \n" +
                "  font-size: 14px; \n" +
                "  text-align: center;\n" +
                "  vertical-align: top; \n" +
                "}\n" +
                ".btn-primary td a {\n" +
                "  background-color: #348eda;\n" +
                "  border: solid 1px #348eda;\n" +
                "  border-radius: 25px;\n" +
                "  border-width: 10px 20px;\n" +
                "  display: inline-block;\n" +
                "  color: #ffffff;\n" +
                "  cursor: pointer;\n" +
                "  font-weight: bold;\n" +
                "  line-height: 2;\n" +
                "  text-decoration: none;\n" +
                "}\n" +
                ".last {\n" +
                "  margin-bottom: 0;\n" +
                "}\n" +
                ".first {\n" +
                "  margin-top: 0;\n" +
                "}\n" +
                ".padding {\n" +
                "  padding: 10px 0;\n" +
                "}\n" +
                "/* -------------------------------------\n" +
                "    BODY\n" +
                "------------------------------------- */\n" +
                "table.body-wrap {\n" +
                "  padding: 20px;\n" +
                "  width: 100%;\n" +
                "}\n" +
                "table.body-wrap .container {\n" +
                "  border: 1px solid #f0f0f0;\n" +
                "}\n" +
                "/* -------------------------------------\n" +
                "    FOOTER\n" +
                "------------------------------------- */\n" +
                "table.footer-wrap {\n" +
                "  clear: both !important;\n" +
                "  width: 100%;  \n" +
                "}\n" +
                ".footer-wrap .container p {\n" +
                "  color: #666666;\n" +
                "  font-size: 12px;\n" +
                "  \n" +
                "}\n" +
                "table.footer-wrap a {\n" +
                "  color: #999999;\n" +
                "}\n" +
                "/* -------------------------------------\n" +
                "    TYPOGRAPHY\n" +
                "------------------------------------- */\n" +
                "\n" +
                "td {\n" +
                " font-size: 13px; \n" +
                "}\n" +
                "h1, \n" +
                "h2, \n" +
                "h3 {\n" +
                "  color: #111111;\n" +
                "  font-family: \"Helvetica Neue\", Helvetica, Arial, \"Lucida Grande\", sans-serif;\n" +
                "  font-weight: 200;\n" +
                "  line-height: 1.2em;\n" +
                "  margin: 40px 0 10px;\n" +
                "}\n" +
                "h1 {\n" +
                "  font-size: 36px;\n" +
                "}\n" +
                "h2 {\n" +
                "  font-size: 28px;\n" +
                "}\n" +
                "h3 {\n" +
                "  font-size: 22px;\n" +
                "}\n" +
                "p, \n" +
                "ul, \n" +
                "ol {\n" +
                "  font-size: 14px;\n" +
                "  font-weight: normal;\n" +
                "  margin-bottom: 10px;\n" +
                "}\n" +
                "ul li, \n" +
                "ol li {\n" +
                "  margin-left: 5px;\n" +
                "  list-style-position: inside;\n" +
                "}\n" +
                "/* ---------------------------------------------------\n" +
                "    RESPONSIVENESS\n" +
                "------------------------------------------------------ */\n" +
                "/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n" +
                ".container {\n" +
                "  clear: both !important;\n" +
                "  display: block !important;\n" +
                "  Margin: 0 auto !important;\n" +
                "  max-width: 600px !important;\n" +
                "}\n" +
                "/* Set the padding on the td rather than the div for Outlook compatibility */\n" +
                ".body-wrap .container {\n" +
                "  padding: 20px;\n" +
                "}\n" +
                "/* This should also be a block element, so that it will fill 100% of the .container */\n" +
                ".content {\n" +
                "  display: block;\n" +
                "  margin: 0 auto;\n" +
                "  max-width: 600px;\n" +
                "}\n" +
                "/* Let's make sure tables in the content area are 100% wide */\n" +
                ".content table {\n" +
                "  width: 100%;\n" +
                "}\n" +
                "</style>\n" +
                "</head>\n" +
                "\n");

        return css;
    }

    private StringBuffer getBodyTop() {
        StringBuffer top = new StringBuffer();
        top.append("<body bgcolor=\"#f6f6f6\">\n" +
                "\t\n" +
                "<!-- body -->\n" +
                "<table class=\"body-wrap\" bgcolor=\"#f6f6f6\">\n" +
                " \n" +
                "  <tr><td></td>\n" +
                "    <td class=\"container\" bgcolor=\"#FFFFFF\">\n" +
                "      <!-- content -->\n" +
                "      <div class=\"content\">\n" +
                "      <table>\n" +
                "\t   <tr>\n" +
                "\t\t  <td colspan=\"3\">\n" +
                "\t\t  <img src=\"https://s3-us-west-2.amazonaws.com/aymerimages/aymer-yapi.png\" style=\"max-height:60px;\">\n" +
                "\t\t  </td>\n" +
                "\t   </tr>\n");

        return top;
    }

    private StringBuffer getDynamicClassicPart(MailSender mailSender) {
        StringBuffer dynamicPart = new StringBuffer();
        dynamicPart.append("        <tr>\n" +
                "          <td colspan=\"4\">\n" +
                "          <p><b>Sayin " +
                mailSender.getKullanici().getAdSoyad() +
                ",</b></p>\n" +
                "\t\t \n" +
                "\t\t " + mailSender.getMesaj() + "\n" +
                "\t\n" +
                "          </td>\n" +
                "        </tr>\n");
        return dynamicPart;
    }

    private StringBuffer getBodyButton() {
        StringBuffer button = new StringBuffer();
        button.append("\t\t  <!-- logolar -->\t\n" +
                "\t\t <tr>\n" +
                "          <td colspan=\"3\">\n" +
                "\t\t  <br/><hr/><br/>\n" +
                "\t\t   <h4>Bilgilendirme,</h4>\n" +
                "\t\t  <table>  \n" +
                "\t\t\t<tr>\n" +
                "\t\t\t\t<td colspan=\"2\">\n" +
                "\t\t\t\t\t\tBu mesaj size Aymer Yapi adina <b>www.aymeryapi.online</b> tarafindan gonderilmistir. \n" +
                "\t\t\t\t</td>\n" +
                "\t\t\t</tr>\t\n" +
                "\t\t\t<tr>\n" +
                "\t\t\t\t<td style=\"width:20%\">\n" +
                "\t\t\t\t\t<b>Tel:</b> \n" +
                "\t\t\t\t</td>\n" +
                "\t\t\t\t<td align=\"left\">\n" +
                "\t\t\t\t    0 312 311 83 95\n" +
                "\t\t\t\t</td>\n" +
                "\t\t\t</tr>\n" +
                "\t\t\t<tr>\n" +
                "\t\t\t\t<td style=\"width:20%\">\n" +
                "\t\t\t\t\t<b>E-posta:</b> \n" +
                "\t\t\t\t</td>\n" +
                "\t\t\t\t<td align=\"left\">\n" +
                "\t\t\t\t\t<a href=\"http://aymer@aymeryapi.com.tr\">aymer@aymeryapi.com.tr</a>   \n" +
                "\t\t\t    </td>\n" +
                "\t\t\t</tr>\n" +
                "\t\t\t\n" +
                "\t\t    <tr>\n" +
                "\t\t\t\t<td style=\"width:20%\">\n" +
                "\t\t\t\t\t<b>Web: </b>\n" +
                "\t\t\t\t</td>\n" +
                "\t\t\t\t<td align=\"left\">\n" +
                "\t\t\t\t    <a href=\"http://www.aymeryapi.com.tr\">www.aymeryapi.com.tr</a>\t\t   \n" +
                "\t\t\t\t</td>\n" +
                "\t\t\t</tr>\n" +
                "\t\t\t\n" +
                "\t\t\t<tr>\n" +
                "\t\t\t\t<td style=\"width:20%\">\n" +
                "\t\t\t\t\t<b>Online Islemler:</b> \n" +
                "\t\t\t\t</td>\n" +
                "\t\t\t\t<td align=\"left\">\n" +
                "\t\t\t\t\t<a href=\"http://www.aymeryapi.online\">www.aymeryapi.online</a>\t\t   \n" +
                "\t\t\t   </td>\n" +
                "\t\t\t</tr>\n" +
                "\t\t\t\n" +
                "\t\t  </table>\n" +
                "            <!-- /button -->\n" +
                " \n" +
                "\t\t    </td>\n" +
                "        </tr>\n" +
                "\t    <tr>\n" +
                "            <td style=\"width:50%\" align=\"center\">\n" +
                "\t\t       <img src=\"https://s3-us-west-2.amazonaws.com/aymerimages/kare.png\" style=\"max-height:30px;\">\n" +
                "\t\t    </td>\n" +
                "\t\t\t<td style=\"width:50%\" align=\"center\">\n" +
                "\t\t       <img src=\"https://s3-us-west-2.amazonaws.com/aymerimages/turkuaz.png\" style=\"max-height:30px;\">\n" +
                "\t\t    </td>\n" +
                "        </tr>\n" +
                "      </table>\n" +
                "      </div>\n" +
                "      <!-- /content -->\n" +
                "      \n" +
                "    </td>\n" +
                "    <td></td>\n" +
                "  </tr>\n" +
                "</table>\n" +
                "<!-- /body -->\n" +
                "\n" +
                "<!-- footer -->\n" +
                "<table class=\"footer-wrap\">\n" +
                "  <tr>\n" +
                "    <td></td>\n" +
                "    <td class=\"container\">\n" +
                "      \n" +
                "      <!-- content -->\n" +
                "      <div class=\"content\">\n" +
                "        <table>\n" +
                "          <tr>\n" +
                "            <td align=\"center\">\n" +
                "              <p>Copyright Aymer Bilisim @2016. <a href=\"http:\\\\www.aymerbilisim.com\"><unsubscribe>www.aymerbilisim.com</unsubscribe></a>\n" +
                "              </p>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </table>\n" +
                "      </div>\n" +
                "      <!-- /content -->\n" +
                "      \n" +
                "    </td>\n" +
                "    <td></td>\n" +
                "  </tr>\n" +
                "</table>\n" +
                "<!-- /footer -->\n" +
                "\n" +
                "</body>\n" +
                "</html>");
        return button;
    }

    public StringBuffer getEMailMessage(MailSender mailSender) {
        StringBuffer message = new StringBuffer();
        message.append(getCssPart());
        message.append(getBodyTop());
        message.append(getDynamicClassicPart(mailSender));
        message.append(getBodyButton());
        return message;
    }

}
