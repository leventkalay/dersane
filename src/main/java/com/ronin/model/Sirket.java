package com.ronin.model;

import com.ronin.model.Interfaces.IAbstractEntity;
import com.ronin.model.constant.Durum;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "sirket")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Audited
public class Sirket implements IAbstractEntity {

    public enum ENUM {
        ////Buradaki siralama veri tabanindaki siralamadir.

        VARSAYILAN_SIRKET(1L,"durum.aktif");

        private Long id;
        private String label;

        private ENUM(Long id,String label){
            this.id=id;
            this.label=label;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    public Sirket() {
    }

    public Sirket(ENUM e) {
        this.id = e.getId();
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "sirket_adi")
    private String sirketAdi;

    @Column(name = "sirket_logo")
    private String sirketLogo;

    @Column(name = "facebook_id")
    private Long facebookId;

    @Column(name = "twitter_tag")
    private String twitterTag;

    @JoinColumn(name = "durum_id", referencedColumnName = "id")
    @ManyToOne
    private Durum durum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSirketAdi() {
        return sirketAdi;
    }

    public void setSirketAdi(String sirketAdi) {
        this.sirketAdi = sirketAdi;
    }

    public String getSirketLogo() {
        return sirketLogo;
    }

    public void setSirketLogo(String sirketLogo) {
        this.sirketLogo = sirketLogo;
    }

    public Durum getDurum() {
        return durum;
    }

    public void setDurum(Durum durum) {
        this.durum = durum;
    }

    @Override
    public String getKisaAciklama() {
        return sirketAdi;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getAciklama() {
        return sirketAdi;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Long getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(Long facebookId) {
        this.facebookId = facebookId;
    }

    public String getTwitterTag() {
        return twitterTag;
    }

    public void setTwitterTag(String twitterTag) {
        this.twitterTag = twitterTag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sirket)) return false;

        Sirket sirket = (Sirket) o;

        if (!id.equals(sirket.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}