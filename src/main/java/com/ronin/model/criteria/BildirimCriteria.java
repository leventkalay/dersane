package com.ronin.model.criteria;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.model.MailSender;
import com.ronin.model.constant.BildirimTipi;
import com.ronin.model.kriter.BildirimSorguKriteri;
import com.ronin.model.kriter.BildirimTipiSorguKriteri;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by msevim on 19.03.2014.
 */
public class BildirimCriteria {

    BildirimSorguKriteri sorguKriteri;

    Session session;
    int first;
    int pageSize;
    SessionInfo sessionInfo;

    public List<MailSender> prepareResult() {

        Criteria cr = session.createCriteria(MailSender.class, "ms");
        cr.setFirstResult(first);
        cr.setMaxResults(pageSize);

        if (sorguKriteri.getBildirimTipi() != null) {
            cr.add(Restrictions.eq("ms.bildirimTipi.id", sorguKriteri.getBildirimTipi().getId()));
        }

        if (sorguKriteri.getDurum() != null) {
            cr.add(Restrictions.eq("ms.durum.id", sorguKriteri.getDurum().getId()));
        }

        if (sorguKriteri.getKullanici() != null) {
            cr.add(Restrictions.eq("ms.kullanici.id", sorguKriteri.getKullanici().getId()));
        }

        return cr.list();
    }

    public BildirimCriteria(BildirimSorguKriteri sorguKriteri, SessionInfo sessionInfo, Session session, int first, int pageSize) {
        this.sorguKriteri = sorguKriteri;
        this.session = session;
        this.first = first;
        this.pageSize = pageSize;
        this.sessionInfo = sessionInfo;
    }
}
