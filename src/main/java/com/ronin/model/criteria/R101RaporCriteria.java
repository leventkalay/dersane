package com.ronin.model.criteria;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.model.kriter.RaporSorguKriteri;
import com.ronin.model.sorguSonucu.R101SorguSonucu;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ealtun on 13.03.2014.
 */
public class R101RaporCriteria {

    RaporSorguKriteri sorguKriteri;
    Session session;
    SessionInfo sessionInfo;

    public List<R101SorguSonucu> prepareResult() {
        List<R101SorguSonucu> resultList = new ArrayList<>();

        String sql = "SELECT \n" +
                "    x.sorumlu_personel,\n" +
                "    x.firma_adi,\n" +
                "    x.toplam_satis_miktari,\n" +
                "    x.toplam_Satis_tutari,\n" +
                "    x.toplam_maliyet,\n" +
                "    x.toplam_kar,\n" +
                "    x.kar_orani\n" +
                "FROM\n" +
                "    (\n" +
                "     SELECT \n" +
                "        f.sorumlu_personel,\n" +
                "            f.firma_adi,\n" +
                "            SUM(st.miktar) AS toplam_satis_miktari,\n" +
                "            ROUND(SUM(st.cikis_net_toplam_maliyet), 2) AS toplam_Satis_tutari,\n" +
                "            ROUND(SUM(st.toplam_alis_maliyet), 2) AS toplam_maliyet,\n" +
                "            ROUND(SUM(st.satis_kari), 2) AS toplam_kar,\n" +
                "            ROUND((ROUND(SUM(st.satis_kari), 3) / ROUND(SUM(st.cikis_net_toplam_maliyet), 3) * 100), 2) AS kar_orani\n" +
                "    FROM\n" +
                "        satis st\n" +
                "    JOIN firma f ON st.firma_id = f.id\n" +
                "    JOIN urun u ON u.id = st.urun_id\n" +
                "    LEFT JOIN seri ser ON ser.id = u.seri_id\n" +
                "    LEFT JOIN model md ON md.id = ser.model_id\n" +
                "    LEFT JOIN marka mar ON mar.id = md.marka_id\n" +
                "    WHERE 1 = 1 " +
                "    and f.sorumlu_personel is not null ";

        if (sorguKriteri.getPersonelAdi() != null) {
            sql += "    and f.sorumlu_personel = :sorumlu_personel_adi\n";
        }
        if (sorguKriteri.getBaslangicTarihi() != null) {
            sql += "    and st.islem_tarihi >= :baslangic_tarihi\n";
        }
        if (sorguKriteri.getBitisTarihi() != null) {
            sql += "    and st.islem_tarihi <= :bitis_tarihi\n";
        }

        sql += "    GROUP BY f.sorumlu_personel , f.firma_adi\n" +
                "    ) x\n" +
                "ORDER BY x.sorumlu_personel asc ,X.toplam_Satis_tutari DESC";

        SQLQuery query = session.createSQLQuery(sql);
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);

        if (sorguKriteri.getPersonelAdi() != null) {
            query.setParameter("sorumlu_personel_adi", sorguKriteri.getPersonelAdi());
        }
        if (sorguKriteri.getBaslangicTarihi() != null) {
            query.setParameter("baslangic_tarihi", sorguKriteri.getBaslangicTarihi());
        }
        if (sorguKriteri.getBitisTarihi() != null) {
            query.setParameter("bitis_tarihi", sorguKriteri.getBitisTarihi());
        }

        List data = query.list();

        for (Object object : data) {
            Map row = (Map) object;
            R101SorguSonucu entiy = new R101SorguSonucu();
            entiy.setPersonelAdi((String) row.get("sorumlu_personel"));
            entiy.setBayiAdi((String) row.get("firma_adi"));
            entiy.setToplamSatisAdedi(row.get("toplam_satis_miktari") != null ? Integer.parseInt(row.get("toplam_satis_miktari").toString()) : 0);
            entiy.setToplamSatisTutari(row.get("toplam_Satis_tutari") != null ? new Double(row.get("toplam_Satis_tutari").toString()) : 0);
            entiy.setToplamMaliyetTutari(row.get("toplam_maliyet") != null ? new Double(row.get("toplam_maliyet").toString()) : 0);
            entiy.setToplamKar(row.get("toplam_kar") != null ? new Double(row.get("toplam_kar").toString()) : 0);
            entiy.setKarOrani(row.get("kar_orani") != null ? new Double(row.get("kar_orani").toString()) : 0);
            resultList.add(entiy);
        }


        return resultList;

    }


    public R101RaporCriteria(RaporSorguKriteri sorguKriteri, SessionInfo sessionInfo, Session session) {
        this.sorguKriteri = sorguKriteri;
        this.session = session;
        this.sessionInfo = sessionInfo;
    }
}
