package com.ronin.model.criteria;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.model.Interfaces.IBaseEntity;
import com.ronin.model.Kitap;
import com.ronin.model.MailSender;
import com.ronin.model.kriter.BildirimSorguKriteri;
import com.ronin.model.kriter.KitapSorguKriteri;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by msevim on 19.03.2014.
 */
public class KitapCriteria {

    KitapSorguKriteri sorguKriteri;

    Session session;
    int first;
    int pageSize;
    SessionInfo sessionInfo;

    public List<IBaseEntity> prepareResult() {

        Criteria cr = session.createCriteria(Kitap.class, "k");
        cr.setFirstResult(first);
        cr.setMaxResults(pageSize);

        if (!StringUtils.isEmpty(sorguKriteri.getAd())) {
            cr.add(Restrictions.like("k.ad", "%" + sorguKriteri.getAd() + "%"));
        }

        if (sorguKriteri.getDil() != null) {
            cr.add(Restrictions.eq("k.dil.id", sorguKriteri.getDil().getId()));
        }
        if (sorguKriteri.getDurum() != null) {
            cr.add(Restrictions.eq("k.durum.id", sorguKriteri.getDurum().getId()));
        }

        return cr.list();
    }

    public KitapCriteria(KitapSorguKriteri sorguKriteri, SessionInfo sessionInfo, Session session, int first, int pageSize) {
        this.sorguKriteri = sorguKriteri;
        this.session = session;
        this.first = first;
        this.pageSize = pageSize;
        this.sessionInfo = sessionInfo;
    }
}
