package com.ronin.model;

import com.ronin.common.model.AbstractEntity;
import com.ronin.common.model.Kullanici;
import com.ronin.model.Interfaces.IBaseEntity;
import com.ronin.model.constant.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "KITAP")
public class Kitap extends AbstractEntity {

    public Kitap() {
    }

    @Column(name = "ad", length = 3096)
    private String ad;

    @Column(name = "yazar", length = 1024)
    private String yazar;

    @Column(name = "yayin_evi", length = 1024)
    private String yayinEvi;

    @JoinColumn(name = "dil_id", referencedColumnName = "id")
    @ManyToOne
    private Dil dil;

    @Column(name = "basim_tarihi")
    private Date basimTarihi;

    @JoinColumn(name = "durum_id", referencedColumnName = "id")
    @ManyToOne
    private Durum durum;


    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getYazar() {
        return yazar;
    }

    public void setYazar(String yazar) {
        this.yazar = yazar;
    }

    public String getYayinEvi() {
        return yayinEvi;
    }

    public void setYayinEvi(String yayinEvi) {
        this.yayinEvi = yayinEvi;
    }

    public Dil getDil() {
        return dil;
    }

    public void setDil(Dil dil) {
        this.dil = dil;
    }

    public Date getBasimTarihi() {
        return basimTarihi;
    }

    public void setBasimTarihi(Date basimTarihi) {
        this.basimTarihi = basimTarihi;
    }

    public Kullanici getEkleyenKullanici() {
        return ekleyenKullanici;
    }

    public void setEkleyenKullanici(Kullanici kullanici) {
        this.ekleyenKullanici = kullanici;
    }

    public Date getIslemTarihi() {
        return islemTarihi;
    }

    public void setIslemTarihi(Date islemTarihi) {
        this.islemTarihi = islemTarihi;
    }

    public Durum getDurum() {
        return durum;
    }

    public void setDurum(Durum durum) {
        this.durum = durum;
    }

    public Kullanici getGuncelleyenKullanici() {
        return guncelleyenKullanici;
    }

    public void setGuncelleyenKullanici(Kullanici guncelleyenKullanici) {
        this.guncelleyenKullanici = guncelleyenKullanici;
    }

    public Date getGuncellemeTarihi() {
        return guncellemeTarihi;
    }

    public void setGuncellemeTarihi(Date guncellemeTarihi) {
        this.guncellemeTarihi = guncellemeTarihi;
    }
}