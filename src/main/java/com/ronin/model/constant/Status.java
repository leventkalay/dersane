package com.ronin.model.constant;

/**
 * Created by levent on 4/1/2017.
 */
public class Status {

    public enum EnumStatus {
        DEVAM_EDIYOR("DEVAM EDIYOR"), TAMAMLANDI("TAMAMLANDI)");

        private final String gosterimMetni;

        private EnumStatus(final String gosterimMetni) {
            this.gosterimMetni = gosterimMetni;
        }

        @Override
        public String toString() {
            return this.gosterimMetni;
        }
    }
}

