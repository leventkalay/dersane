package com.ronin.model.constant;

import com.ronin.common.model.KullaniciRol;
import com.ronin.model.BildirimTipiRol;
import com.ronin.model.Interfaces.IAbstractEntity;
import org.hibernate.annotations.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by fcabi on 09.03.2014.
 */

@Entity
@Table(name = "bildirim_tipi")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "BildirimTipi.findAll", query = "SELECT b FROM BildirimTipi b")})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Audited
public class BildirimTipi implements IAbstractEntity {
    public enum ENUM {
        ////Buradaki siralama veri tabanindaki siralamadir.

        KITAP_BILDIRIM(1L,"durum.aktif"),
        SIPARIS_ONAY(2L,"durum.aktif"),
        DUYURU(3L,"durum.aktif"),
        ILAN_EKLEME(4L,"durum.aktif"),
        SIKAYET(5L,"durum.aktif"),
        BELGE_TALEBI(6L,"durum.aktif"),
        ANKET(7L,"durum.aktif"),
        PASSWORD_RESET(8L,"durum.aktif"),
        SIPARIS(9L,"durum.aktif");

        private Long id;
        private String label;

        private ENUM(Long id,String label){
            this.id=id;
            this.label=label;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    public BildirimTipi(ENUM e) {
        this.id = e.getId();
    }

    public BildirimTipi() {
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "kisa_aciklama")
    private String kisaAciklama;
    @Column(name = "aciklama")
    private String aciklama;

    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "bildirimTipi",fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private List<BildirimTipiRol> bildirimTipiRolList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKisaAciklama() {
        return kisaAciklama;
    }

    public void setKisaAciklama(String kisaAciklama) {
        this.kisaAciklama = kisaAciklama;
    }

    public String getAciklama() {
        return aciklama;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public boolean isSiparisMi(){
        return this.getId().equals(ENUM.SIPARIS.getId());
    }

    public boolean isSiparisOnayMi(){
        return this.getId().equals(ENUM.SIPARIS_ONAY.getId());
    }

    public boolean isDuyuruMu(){
        return this.getId().equals(ENUM.DUYURU.getId());
    }

    public boolean isIlanEklemeMi(){
        return this.getId().equals(ENUM.ILAN_EKLEME.getId());
    }

    public boolean isSikayetMi(){
        return this.getId().equals(ENUM.SIKAYET.getId());
    }

    public boolean isBelgeTalepMi(){
        return this.getId().equals(ENUM.BELGE_TALEBI.getId());
    }

    public boolean isAnketMi(){
        return this.getId().equals(ENUM.ANKET.getId());
    }

    public List<BildirimTipiRol> getBildirimTipiRolList() {
        return bildirimTipiRolList;
    }

    public void setBildirimTipiRolList(List<BildirimTipiRol> bildirimTipiRolList) {
        this.bildirimTipiRolList = bildirimTipiRolList;
    }

    public static BildirimTipi getAktifKitapBildirimTipi() {
       return new BildirimTipi(ENUM.KITAP_BILDIRIM);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BildirimTipi)) return false;

        BildirimTipi bildirimTipi = (BildirimTipi) o;

        if (id != null ? !id.equals(bildirimTipi.id) : bildirimTipi.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
