package com.ronin.model;

import com.ronin.common.model.Kullanici;
import com.ronin.model.Interfaces.IAbstractEntity;
import com.ronin.model.constant.Durum;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: ealtun
 * Date: 27.03.2014
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "veri_aktarma_istek")
public class VeriAktarmaIstek  {

    @Id
    @GeneratedValue
    @Column(name = "id",length = 10)
    private Long id;

    @Column(name = "personel_adi",length = 128)
    private String personelAdi;

    @Column(name = "firma_adi",length = 512)
    private String firmaAdi;

    @Column(name = "firma_kodu",length = 512)
    private String firmaKodu;

    @Column(name = "mal_kodu",length = 128)
    private String malKodu;

    @Column(name = "mal_adi",length = 512)
    private String malAdi;

    @Column(name = "islem_tarihi",length = 20)
    private Date islemTarihi;

    @Column(name = "faturaNo",length = 128)
    private String faturaNo;

    @Column(name = "miktar")
    private Integer miktar;

    @Column(name = "birimMaliyet")
    private Double birimMaliyet;

    @Column(name = "cikis_net_birim_maliyet")
    private Double cikisNetBirimMaliyet;

    @Column(name = "cikis_net_toplam_maliyet")
    private Double cikisNetToplamMaliyet;

    @Column(name = "toplam_alis_maliyet")
    private Double toplamAlisMaliyet;

    @Column(name = "satis_kari")
    private Double satisKari;

    @JoinColumn(name = "durum_id", referencedColumnName = "id")
    @ManyToOne
    private Durum durum;

    @ManyToOne
    @JoinColumn(name = "SIRKET_ID", referencedColumnName = "id")
    @Fetch(FetchMode.SELECT)
    private Sirket sirket;

    @Column(name = "aktarim_tipi",length = 512)
    private Integer aktarimTipi;


    public VeriAktarmaIstek() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirmaAdi() {
        return firmaAdi;
    }

    public void setFirmaAdi(String firmaAdi) {
        this.firmaAdi = firmaAdi;
    }

    public String getMalKodu() {
        return malKodu;
    }

    public void setMalKodu(String malKodu) {
        this.malKodu = malKodu;
    }

    public String getMalAdi() {
        return malAdi;
    }

    public void setMalAdi(String malAdi) {
        this.malAdi = malAdi;
    }

    public Date getIslemTarihi() {
        return islemTarihi;
    }

    public void setIslemTarihi(Date islemTarihi) {
        this.islemTarihi = islemTarihi;
    }

    public String getFaturaNo() {
        return faturaNo;
    }

    public void setFaturaNo(String faturaNo) {
        this.faturaNo = faturaNo;
    }

    public Integer getMiktar() {
        return miktar;
    }

    public void setMiktar(Integer miktar) {
        this.miktar = miktar;
    }

    public Double getBirimMaliyet() {
        return birimMaliyet;
    }

    public void setBirimMaliyet(Double birimMaliyet) {
        this.birimMaliyet = birimMaliyet;
    }

    public Double getCikisNetBirimMaliyet() {
        return cikisNetBirimMaliyet;
    }

    public void setCikisNetBirimMaliyet(Double cikisNetBirimMaliyet) {
        this.cikisNetBirimMaliyet = cikisNetBirimMaliyet;
    }

    public Double getCikisNetToplamMaliyet() {
        return cikisNetToplamMaliyet;
    }

    public void setCikisNetToplamMaliyet(Double cikisNetToplamMaliyet) {
        this.cikisNetToplamMaliyet = cikisNetToplamMaliyet;
    }

    public Double getToplamAlisMaliyet() {
        return toplamAlisMaliyet;
    }

    public void setToplamAlisMaliyet(Double toplamAlisMaliyet) {
        this.toplamAlisMaliyet = toplamAlisMaliyet;
    }

    public Double getSatisKari() {
        return satisKari;
    }

    public void setSatisKari(Double satisKari) {
        this.satisKari = satisKari;
    }

    public Durum getDurum() {
        return durum;
    }

    public void setDurum(Durum durum) {
        this.durum = durum;
    }

    public Sirket getSirket() {
        return sirket;
    }

    public void setSirket(Sirket sirket) {
        this.sirket = sirket;
    }

    public String getPersonelAdi() {
        return personelAdi;
    }

    public void setPersonelAdi(String personelAdi) {
        this.personelAdi = personelAdi;
    }

    public Integer getAktarimTipi() {
        return aktarimTipi;
    }

    public void setAktarimTipi(Integer aktarimTipi) {
        this.aktarimTipi = aktarimTipi;
    }

    public String getFirmaKodu() {
        return firmaKodu;
    }

    public void setFirmaKodu(String firmaKodu) {
        this.firmaKodu = firmaKodu;
    }
}
