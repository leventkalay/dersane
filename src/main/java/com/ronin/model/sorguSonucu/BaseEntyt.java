package com.ronin.model.sorguSonucu;

import com.ronin.common.model.Kullanici;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * Created by ealtun on 1/23/2017.
 */
public class BaseEntyt {

    @Column(name = "islem_tarihi")
    Date islemTarihi;

    @JoinColumn(name = "islemi_yapan_kullanici_id", referencedColumnName = "id")
    @ManyToOne
    Kullanici islemiYapanKullanici;


    public Date getIslemTarihi() {
        return islemTarihi;
    }

    public void setIslemTarihi(Date islemTarihi) {
        this.islemTarihi = islemTarihi;
    }

    public Kullanici getIslemiYapanKullanici() {
        return islemiYapanKullanici;
    }

    public void setIslemiYapanKullanici(Kullanici islemiYapanKullanici) {
        this.islemiYapanKullanici = islemiYapanKullanici;
    }
}
