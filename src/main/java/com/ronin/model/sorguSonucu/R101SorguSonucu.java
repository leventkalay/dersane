package com.ronin.model.sorguSonucu;

/**
 * Created with IntelliJ IDEA.
 * User: ealtun
 * Date: 14.04.2014
 * Time: 10:52
 * To change this template use File | Settings | File Templates.
 */
public class R101SorguSonucu {

    String personelAdi;
    String bayiAdi;
    Double toplamSatisTutari;
    Integer toplamSatisAdedi;
    Double toplamMaliyetTutari;
    Double toplamKar;
    Double karOrani;

    public String getPersonelAdi() {
        return personelAdi;
    }

    public void setPersonelAdi(String personelAdi) {
        this.personelAdi = personelAdi;
    }

    public String getBayiAdi() {
        return bayiAdi;
    }

    public void setBayiAdi(String bayiAdi) {
        this.bayiAdi = bayiAdi;
    }

    public Double getToplamSatisTutari() {
        return toplamSatisTutari;
    }

    public void setToplamSatisTutari(Double toplamSatisTutari) {
        this.toplamSatisTutari = toplamSatisTutari;
    }

    public Integer getToplamSatisAdedi() {
        return toplamSatisAdedi;
    }

    public void setToplamSatisAdedi(Integer toplamSatisAdedi) {
        this.toplamSatisAdedi = toplamSatisAdedi;
    }

    public Double getToplamMaliyetTutari() {
        return toplamMaliyetTutari;
    }

    public void setToplamMaliyetTutari(Double toplamMaliyetTutari) {
        this.toplamMaliyetTutari = toplamMaliyetTutari;
    }

    public Double getToplamKar() {
        return toplamKar;
    }

    public void setToplamKar(Double toplamKar) {
        this.toplamKar = toplamKar;
    }

    public Double getKarOrani() {
        return karOrani;
    }

    public void setKarOrani(Double karOrani) {
        this.karOrani = karOrani;
    }
}
