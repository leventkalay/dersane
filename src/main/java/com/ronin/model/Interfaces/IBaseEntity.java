package com.ronin.model.Interfaces;

import com.ronin.common.model.Kullanici;

import java.util.Date;

/**
 * Created by levent on 3/15/2017.
 */

public interface IBaseEntity {
    Long getId();

    Kullanici getEkleyenKullanici();

    void setEkleyenKullanici(Kullanici kullanici);

    Date getIslemTarihi();

    void setIslemTarihi(Date islemTarihi);

    Kullanici getGuncelleyenKullanici();

    void setGuncelleyenKullanici(Kullanici guncelleyenKullanici);

    Date getGuncellemeTarihi();

    void setGuncellemeTarihi(Date guncellemeTarihi);

    public Long getVersiyon();

    public void setVersiyon(Long versiyon);
}
