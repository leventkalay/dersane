package com.ronin.utils;
 
import com.ronin.jobs.BildirimOlustur;
import com.ronin.jobs.KitapAtama;
import com.ronin.jobs.MailGonder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
 


@Configuration
@EnableScheduling
public class AppConfig {
 
    @Bean
    public BildirimOlustur bean() {
        return new BildirimOlustur();
    }

    @Bean
    public MailGonder beanMailGonder() {
        return new MailGonder();
    }

    @Bean
    public KitapAtama beanKitapAtama() {
        return new KitapAtama();
    }
 
}