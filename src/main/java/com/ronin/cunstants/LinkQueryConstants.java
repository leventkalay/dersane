package com.ronin.cunstants;


public interface LinkQueryConstants {

    String databaseName = "YNS00018";

    String firmaBilgileriQuery = "select x.CAR002_Row_ID as id,\n" +
            "       x.CAR002_HesapKodu as firmaKodu, \n" +
            "       RTRIM(LTRIM(x.CAR002_Unvan1 + '' + x.CAR002_Unvan2))  as firmaAdi, \n" +
            "       RTRIM(LTRIM(x.CAR002_Adres1 + ' ' + x.CAR002_Adres2 + ' ' + x.CAR002_Adres3)) as adres,\n" +
            "       RTRIM(LTRIM(x.CAR002_Telefon1 + ' ' + x.CAR002_Telefon2 + ' ' + x.CAR002_Telefon3 + ' ' + x.CAR002_Telefon4)) as telefon, \n" +
            "       x.CAR002_OzelKodu as personel_adi,\n" +
            "       (-x.CAR002_DevirBorc - x.CAR002_DigerBorc - x.CAR002_OdemeBorc - x.CAR002_CiroBorc - x.CAR002_IadeBorc - x.CAR002_KDVBorc - x.CAR002_otvborc +\n" +
            "       x.CAR002_DevirAlacak + x.CAR002_DigerAlacak + x.CAR002_OdemeAlacak + x.CAR002_CiroAlacak + x.CAR002_IadeAlacak + x.CAR002_KDVAlacak + x.CAR002_otvalacak) as bakiye , \n" +
            "       (x.CAR002_DevirBorc + x.CAR002_DigerBorc + x.CAR002_OdemeBorc + x.CAR002_CiroBorc + x.CAR002_IadeBorc + x.CAR002_KDVBorc + x.CAR002_otvborc) as toplam_borc , " +
            "       (x.CAR002_DevirAlacak + x.CAR002_DigerAlacak + x.CAR002_OdemeAlacak + x.CAR002_CiroAlacak + x.CAR002_IadeAlacak + x.CAR002_KDVAlacak + x.CAR002_otvalacak) as toplam_alacak " +
            " FROM " + databaseName + "." + databaseName + "." + "CAR002 x " +
            " where x.CAR002_OzelKodu is not null" +
            " and x.CAR002_OzelKodu <> ''";


    String firmaSatislar = " SELECT sat.STK005_Row_ID ,  \n" +
            "                               sat.STK005_CariHesapKodu as firma_kodu,   \n" +
            "                               sat.STK005_MalKodu as urukodu,   \n" +
            "                               DATEADD(DAY, -2, cast(cast(sat.STK005_IslemTarihi as datetime) as date)) as islem_tarihi,  \n" +
            "                               sat.STK005_EvrakSeriNo as evrak_seri_no,  \n" +
            "                               sat.STK005_Miktari as miktar,  \n" +
            "                               (sat.STK005_Tutari - sat.STK005_Iskonto) / sat.STK005_Miktari as birim_satis,  \n" +
            "                               sat.STK005_Tutari - sat.STK005_Iskonto as toplam_Satis,                                 \n" +
            "                               CASE ur.STK004_AlisFiyati3 \n" +
            "                                WHEN 0 THEN ur.STK004_AlisFiyati2 \n" +
            "                                else ur.STK004_AlisFiyati3 \n" +
            "                               END as birim_maliyet,             \n" +
            "                               sat.STK005_Miktari * CASE ur.STK004_AlisFiyati3  WHEN 0 THEN ur.STK004_AlisFiyati2  else ur.STK004_AlisFiyati3   END as toplam_maliyet ,  \n" +
            "                               (sat.STK005_Tutari - sat.STK005_Iskonto) - (sat.STK005_Miktari * CASE ur.STK004_AlisFiyati3  WHEN 0 THEN ur.STK004_AlisFiyati2  else ur.STK004_AlisFiyati3   END) as kar,  \n" +
            "                               ur.STK004_Aciklama as urun_adi,   \n" +
            "                               ur.STK004_DepoGirisMiktari1 - ur.STK004_DepoCikisMiktari1 as kalan_stok  " +
            " FROM " + databaseName + "." + databaseName + "." + "STK005 sat , " + databaseName + "." + databaseName + "." + "CAR002 fir , " + databaseName + "." + databaseName + "." + "STK004 ur\n" +
            " where fir.CAR002_HesapKodu = sat.STK005_CariHesapKodu\n" +
            " and sat.STK005_MalKodu = ur.STK004_MalKodu\n" +
            " and sat.STK005_GC = 1\n" +
            " and sat.STK005_CariHesapKodu = ";

    String senetTahsilatlari = "SELECT senet.SCS002_Row_ID as row_id,\n" +
            "       fir.CAR002_HesapKodu as firma_kodu , \n" +
            "       fir.CAR002_Unvan1 + fir.CAR002_Unvan2 as firma_adi ,   \n" +
            "       CASE senet.SCS002_BorclusuUnvani1\n" +
            "        WHEN '' THEN senet.SCS002_Borclusu\n" +
            "        else senet.SCS002_BorclusuUnvani1 + senet.SCS002_BorclusuUnvani2\n" +
            "       END as borclu_adi,\n" +
            "       senet.SCS002_BorclusuAdres1 + senet.SCS002_BorclusuAdres2 as borclu_adresi,\n" +
            "       senet.SCS002_Tutar as tutar, \n" +
            "       DATEADD(DAY, -2, cast(cast(senet.SCS002_DuzenlendigiTarih as datetime) as date)) as duzenlenme_tarihi,\n" +
            "       senet.SCS002_SenetNo as senet_no,\n" +
            "       DATEADD(DAY, -2, cast(cast(senet.SCS002_VadeTarihi as datetime) as date)) as vade_tarihi\n" +
            " FROM YNS00018.YNS00018.SCS002 senet , YNS00018.YNS00018.CAR002 fir\n" +
            " where senet.SCS002_HesapKodu = fir.CAR002_HesapKodu\n" +
            " and fir.CAR002_HesapKodu = '";

    String cekTahsilatlari = "SELECT cek.SCS003_Row_ID as row_id,\n" +
            "       fir.CAR002_HesapKodu as firma_kodu,\n" +
            "       fir.CAR002_Unvan1 + fir.CAR002_Unvan2 as firma_adi ,  \n" +
            "       cek.SCS003_Banka as banka,\n" +
            "       cek.SCS003_Sube as sube,\n" +
            "       CASE cek.SCS003_DuzenleyenUnvani1\n" +
            "        WHEN '' THEN cek.SCS003_Duzenleyen\n" +
            "        else cek.SCS003_DuzenleyenUnvani1 + cek.SCS003_DuzenleyenUnvani2 \n" +
            "       END as borclu_adi,\n" +
            "       cek.SCS003_Tutar as tutar,\n" +
            "       DATEADD(DAY, -2, cast(cast(cek.SCS003_AlimTarihi as datetime) as date)) as alim_tarihi,\n" +
            "       cek.SCS003_CekNo as cek_no,\n" +
            "       DATEADD(DAY, -2, cast(cast(cek.SCS003_VadeTarihi as datetime) as date)) as vade_tarihi\n" +
            " FROM YNS00018.YNS00018.SCS003 cek , YNS00018.YNS00018.CAR002 fir\n" +
            " where cek.SCS003_HesapKodu = fir.CAR002_HesapKodu\n" +
            " and fir.CAR002_HesapKodu = '";

   String nakitTahsilat = "SELECT nakit.CAR003_Row_ID as row_id,\n" +
           "       fir.CAR002_HesapKodu as firma_kodu,\n" +
           "       fir.CAR002_Unvan1 + fir.CAR002_Unvan2 as firma_adi,\n" +
           "       nakit.CAR003_Tutar as tutar,\n" +
           "       nakit.CAR003_EvrakSeriNo,\n" +
           "       DATEADD(DAY, -2, cast(cast(nakit.CAR003_Tarih as datetime) as date)) as alim_tarihi " +
           " FROM YNS00018.YNS00018.CAR003 nakit , YNS00018.YNS00018.CAR002 fir\n" +
           " where nakit.CAR003_HesapKodu = fir.CAR002_HesapKodu\n" +
           " and fir.CAR002_OzelKodu is not null\n" +
           " and nakit.CAR003_IslemTipi = 1\n" +
           " and fir.CAR002_HesapKodu = '";

    String stokKarti = "SELECT ur.STK004_Row_ID as row_id,\n" +
            "       ur.STK004_MalKodu as urun_kodu,\n" +
            "       ur.STK004_Aciklama as urun_adi,\n" +
            "       ur.STK004_SatisFiyati1 as fiyat_1,\n" +
            "       ur.STK004_SatisFiyati2 as fiyat_2,\n" +
            "       ur.STK004_SatisFiyati3 as fiyat_3,\n" +
            "       ur.STK004_DepoGirisMiktari1 - ur.STK004_DepoCikisMiktari1 as kalan_stok,\n" +
            "       DATEADD(DAY, -2, cast(cast(ur.STK004_SonCikisTarihi as datetime) as date)) as son_Satis_tarihi\n" +
            " FROM YNS00018.YNS00018.STK004 ur";




    // TODO: 2/15/2017 buras? eksik daha sonra d�?�n�l�p entegre edilebilir , aymerin yazd?g? �ekler
    String yazilanCekler = "SELECT cek_ur.SCS005_Row_ID as row_id ,\n" +
            "       fir.CAR002_HesapKodu as firma_kodu,\n" +
            "       fir.CAR002_Unvan1 + fir.CAR002_Unvan2 as firma_adi,\n" +
            "       cek_ur.SCS005_Banka as banka,\n" +
            "       cek_ur.SCS005_Sube as sube,\n" +
            "       cek_ur.SCS005_Tutar as tutar,\n" +
            "       cek_ur.SCS005_CekNo as cek_no,\n" +
            "       cast(cast(cek_ur.SCS005_VadeTarihi as datetime) as date)  as vade_tarihi\n" +
            " FROM YNS00018.YNS00018.SCS005 cek_ur , YNS00018.YNS00018.CAR002 fir\n" +
            " where cek_ur.SCS005_HesapKodu = fir.CAR002_HesapKodu\n" +
            " and fir.CAR002_HesapKodu = ";



}