package com.ronin.jobs;

import com.ronin.common.service.IKitapService;
import com.ronin.model.Kitap;
import com.ronin.model.constant.BildirimTipi;
import com.ronin.service.IBildirimService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by levent on 3/29/2017.
 */

@Component
public class KitapAtama {
    @Autowired
    private IKitapService kitapService;

    @Autowired
    private IBildirimService bildirimService;

    @Scheduled(fixedRate=300000000)
    public void atamayiGerceklestir() {
        List<Kitap> KitapListesi = kitapService.aktifKitapGetir();

        for (Kitap kitap : KitapListesi) {
            bildirimService.addEmailIstek(BildirimTipi.getAktifKitapBildirimTipi(),
                    kitap.getEkleyenKullanici(),
                                            "mesaj",
                                            "konu",kitap);
        }
    }

//    private void mailGonder(KullaniciKitap kullaniciKitap) throws Exception {
//        //StringBuffer emailMessage = bildirimService.getMailMessage(mailSender);
//        StringBuffer emailMessage = new StringBuffer("Mail  Detay?");
//        Multipart multipart = getMessageMultipart(emailMessage.toString());
//        mailService.setUsername("no-reply@abc.com.tr");
//        mailService.setPassword("1234");
//        MimeMessage message = mailService.createMimeMessage();
//        message.addFrom(InternetAddress.parse("no-reply@abc.com.tr"));
//        message.addRecipient(Message.RecipientType.TO, new InternetAddress(kullaniciKitap.getKullanici().getEmail()));
//        message.setSubject("Okumakta oldgunuz kitaplar hakk?nda", "utf-8");
//        message.setContent(multipart);
//        message.setContentLanguage(new String[]{"TR"});
//        mailService.send(message);
//    }
//
//    private Multipart getMessageMultipart(String messageBody) throws Exception {
//        BodyPart messageBodyPart = new MimeBodyPart();
//        messageBodyPart.setContent(messageBody, "text/html; charset=UTF-8");
//        Multipart multipart = new MimeMultipart("alternative");
//        multipart.addBodyPart(messageBodyPart);
//        return multipart;
//    }
}
