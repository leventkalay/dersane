package com.ronin.jobs;

import com.ronin.model.MailSender;
import com.ronin.service.IBildirimService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.List;


/**
 * Created by mcan on 07/04/2015.
 */

@Component
public class MailGonder {

    @Autowired
    private JavaMailSenderImpl mailService;

    @Autowired
    private IBildirimService bildirimService;


    @Scheduled(fixedRate=600000000)
    public void execute() {
        List<MailSender> mailSenderList = bildirimService.getMailSender(null);

        if (mailSenderList != null) {
            for (MailSender mailSender : mailSenderList) {
                try {
                    StringBuffer emailMessage = bildirimService.getMailMessage(mailSender);
                    Multipart multipart = getMessageMultipart(emailMessage.toString());
                    mailService.setUsername("no-reply@aymeryapi.com.tr");
                    mailService.setPassword("Aymr1350");
                    MimeMessage message = mailService.createMimeMessage();
                    message.addFrom(InternetAddress.parse("no-reply@aymeryapi.com.tr"));
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(mailSender.getKullanici().getEmail()));
                    message.setSubject(mailSender.getSubject(), "utf-8");
                    message.setContent(multipart);
                    message.setContentLanguage(new String[]{"TR"});
                    mailService.send(message);
                    // mail kayd� pasife cekiliyor.
                    bildirimService.updateMailSenderPasif(mailSender);
                } catch (Exception ex) {
                    bildirimService.updateMailSenderPasif(mailSender);
                    ex.printStackTrace();
                }
            }
        }
    }


    private Multipart getMessageMultipart(String messageBody) throws Exception {
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(messageBody, "text/html; charset=UTF-8");
        Multipart multipart = new MimeMultipart("alternative");
        multipart.addBodyPart(messageBodyPart);
        return multipart;
    }
    @Scheduled(fixedRate=6000000)
    private void kitapMailiGonder()
    {
        List<MailSender> mailSenderList = bildirimService.getMailSender(null);

        if (mailSenderList != null) {
            for (MailSender mailSender : mailSenderList) {
                try {
                    StringBuffer emailMessage = bildirimService.getMailMessage(mailSender);
                    Multipart multipart = getMessageMultipart(emailMessage.toString());
                    mailService.setUsername("dsada@hotmail.com");
                    mailService.setPassword("dsad");
                    MimeMessage message = mailService.createMimeMessage();
                    message.addFrom(InternetAddress.parse("levent_kalay@hotmail.com"));
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress("levent_kalay@hotmail.com"));
                    message.setSubject(mailSender.getSubject(), "utf-8");
                    message.setContent(multipart);
                    message.setContentLanguage(new String[]{"TR"});
                    mailService.send(message);
                    // mail kayd� pasife cekiliyor.
                    bildirimService.updateMailSenderPasif(mailSender);
                } catch (Exception ex) {
                    bildirimService.updateMailSenderPasif(mailSender);
                    ex.printStackTrace();
                }
            }


        }

    }

}
