package com.ronin.jobs;

import com.ronin.commmon.beans.SessionInfo;
import com.ronin.common.model.Kullanici;
import com.ronin.model.BildirimIstek;
import com.ronin.service.IBildirimService;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedProperty;
import java.security.PrivateKey;
import java.util.List;


/**
 * Created by mcan on 07/04/2015.
 */

@Component
public class BildirimOlustur {


    @Autowired
    private IBildirimService bildirimService;


    @Scheduled(fixedRate=300000)
    public void execute() {
        // i�lenmemi� durumdaki bildirim istekler al�n�r.
        List<BildirimIstek> bildirimIstekList = bildirimService.getIslenmemisBildirimIstek(null);
        // loopp da doneriz
        for (BildirimIstek bildirimIstek : bildirimIstekList) {
            if (bildirimIstek.getBilgilendirmeTipi().isEmail()) {
                if (bildirimIstek.getKullanici() != null) {
                    bildirimService.addEmailIstek(bildirimIstek);
                } else {
                    // kullan�c� null ise kullan�c� listesini buluruz
                    List<Kullanici> kullaniciList = bildirimService.getHedefKitle(bildirimIstek.getBildirimTipi(), null);
                    for (Kullanici kullanici : kullaniciList) {
                        bildirimIstek.setKullanici(kullanici);
                        bildirimService.addEmailIstek(bildirimIstek);
                    }
                }
            } else if (bildirimIstek.getBilgilendirmeTipi().isNotification()) {
                if (bildirimIstek.getKullanici() != null) {
                    bildirimService.addNotification(bildirimIstek);
                } else {
                    // kullan�c� null ise kullan�c� listesini buluruz
                    List<Kullanici> kullaniciList = bildirimService.getHedefKitle(bildirimIstek.getBildirimTipi(), null);
                    for (Kullanici kullanici : kullaniciList) {
                        bildirimIstek.setKullanici(kullanici);
                        bildirimService.addNotification(bildirimIstek);
                    }
                }
            }
            // bildirim istek kayd� pasif yap�l�yor.
            bildirimService.updateBildirimIstekPasif(bildirimIstek);
        }

    }


}
