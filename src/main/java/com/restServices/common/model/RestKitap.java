package com.restServices.common.model;

import com.ronin.model.Kitap;

/**
 * Created by levent on 4/5/2017.
 */
public class RestKitap {


    private Kitap kitap;

    private String kitapAdi;
    private String yazari;


    public RestKitap(String kitapAdi, String yazari) {
        this.kitapAdi = kitapAdi;
        this.yazari = yazari;
    }

    public RestKitap() {
    }

    public String getKitapAdi() {
        return kitapAdi;
    }

    public void setKitapAdi(String kitapAdi) {
        this.kitapAdi = kitapAdi;
    }

    public String getYazari() {
        return yazari;
    }

    public void setYazari(String yazari) {
        this.yazari = yazari;
    }

    public Kitap getKitap() {
        return kitap;
    }

    public void setKitap(Kitap kitap) {
        this.kitap = kitap;
    }
}
