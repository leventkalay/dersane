package com.restServices.common.model;

import java.io.Serializable;

/**
 * Created by ealtun on 16.03.2016.
 */
public class Profile implements Serializable {

    private User user;
    private String password;
    private Boolean isSuccessfull;

    public Profile() {
    }

    public Profile(User user, String password) {
        this.user = user;
        this.password = password;
        isSuccessfull =  false;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getSuccessfull() {
        return isSuccessfull;
    }

    public void setSuccessfull(Boolean successfull) {
        isSuccessfull = successfull;
    }
}
