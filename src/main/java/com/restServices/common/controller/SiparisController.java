package com.restServices.common.controller;

import com.ronin.common.service.IKullaniciService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by fcabi on 06.01.2015.
 */

@RestController
@RequestMapping("/siparis")
public class SiparisController {


    @Autowired
    private IKullaniciService kullaniciService;

//    @RequestMapping(value = "/urunList", method = RequestMethod.POST, produces = "application/json")
//    List<ProductItem> getAllUrunList(@RequestBody ProductCriteria productCriteria) {
//        List<ProductItem> productItems = new ArrayList<>();
//        UrunSorguKriteri urunSorguKriteri = new UrunSorguKriteri();
//        urunSorguKriteri.setDurum(Durum.getAktifObject());
//
//        if (productCriteria != null) {
//            if (productCriteria.getProduct_name() != null && !productCriteria.getProduct_name().trim().isEmpty()) {
//                urunSorguKriteri.setAd(productCriteria.getProduct_name());
//            } else {
//                if (productCriteria.getSeri_id() != null) {
//                    Seri seri = new Seri();
//                    seri.setId(productCriteria.getSeri_id().longValue());
//                    urunSorguKriteri.setSeri(seri);
//                }
//            }
//        }
//        SessionInfo sessionInfo = new SessionInfo();
//        sessionInfo.setSirket(new Sirket(Sirket.ENUM.VARSAYILAN_SIRKET));
//
//        List<Urun> urunList = urunService.getListCriteriaForPaging(0, 100, urunSorguKriteri, sessionInfo);
//
//        for (Urun urun : urunList) {
//            productItems.add(new ProductItem(urun.getId().intValue(),
//                                            urun.getSeri().getId().intValue(),
//                                            urun.getSeri().getSeri() != null ? urun.getSeri().getSeri().getId().intValue()  : null,
//                                            urun.getSeri().getTedarikci().getId().intValue(),
//                                            urun.getSeri().getTedarikci().getAd(),
//                    urun.getSeri().getAd(),
//                                            (urun.getSeri().getSeri() != null ? urun.getSeri().getSeri().getAd() : ""),
//                                            urun.getAd(),
//                                            0,
//                                                0,
//                                            urun.getKod(),
//                                            urun.getAciklama(),
//                                            urun.getImagePath()));
//        }
//        return productItems;
//    }
//
//    @RequestMapping(value = "/tedarikciList", method = RequestMethod.GET, produces = "application/json")
//    List<KategoriItem> getAllTedarikciList() {
//        List<KategoriItem> kategoriItemList = new ArrayList<>();
//        TedarikciSorguKriteri tedarikciSorguKriteri = new TedarikciSorguKriteri();
//        tedarikciSorguKriteri.setDurum(Durum.getAktifObject());
//
//        SessionInfo sessionInfo = new SessionInfo();
//        sessionInfo.setSirket(new Sirket(Sirket.ENUM.VARSAYILAN_SIRKET));
//
//        List<Tedarikci> tedarikciList = urunService.getListCriteriaForPaging(0, 100, tedarikciSorguKriteri, sessionInfo);
//
//        for (Tedarikci tedarikci : tedarikciList) {
//            kategoriItemList.add(new KategoriItem(tedarikci.getId().intValue(), 0, tedarikci.getAd(), tedarikci.getAciklama(), tedarikci.getImagePath()));
//        }
//        return kategoriItemList;
//    }
//
//    @RequestMapping(value = "/seriList", method = RequestMethod.GET, produces = "application/json")
//    List<SeriItem> getAllSeriList() {
//        List<SeriItem> seriItemList = new ArrayList<>();
//        SeriSorguKriteri seriSorguKriteri = new SeriSorguKriteri();
//        seriSorguKriteri.setDurum(Durum.getAktifObject());
//
//        SessionInfo sessionInfo = new SessionInfo();
//        sessionInfo.setSirket(new Sirket(Sirket.ENUM.VARSAYILAN_SIRKET));
//
//        List<Seri> seriList = urunService.getListCriteriaForPaging(0, 100, seriSorguKriteri, sessionInfo);
//
//        for (Seri seri : seriList) {
//            seriItemList.add(new SeriItem(seri.getId().intValue(), seri.getTedarikci().getId().intValue(), seri.getSeri() != null ? seri.getSeri().getId().intValue() : null, seri.getAd(), seri.getAciklama(), seri.getImagePath()));
//        }
//        return seriItemList;
//    }
//
//    @RequestMapping(value = "/seriListWithKategori", method = RequestMethod.POST, produces = "application/json")
//    List<SeriItem> getAllSeriListByKategori(@RequestBody KategoriItem kategoriItem) {
//        Tedarikci tedarikci = new Tedarikci();
//        tedarikci.setId(kategoriItem.getId().longValue());
//
//        List<SeriItem> seriItemList = new ArrayList<>();
//        SeriSorguKriteri seriSorguKriteri = new SeriSorguKriteri();
//        seriSorguKriteri.setDurum(Durum.getAktifObject());
//        seriSorguKriteri.setTedarikci(tedarikci);
//
//        SessionInfo sessionInfo = new SessionInfo();
//        sessionInfo.setSirket(new Sirket(Sirket.ENUM.VARSAYILAN_SIRKET));
//
//        List<Seri> seriList = urunService.getListCriteriaForPaging(0, 100, seriSorguKriteri, sessionInfo);
//
//        for (Seri seri : seriList) {
//            if (seri.getSeri() == null)
//                seriItemList.add(new SeriItem(seri.getId().intValue(), seri.getTedarikci().getId().intValue(), seri.getSeri() != null ? seri.getSeri().getId().intValue() : null, seri.getAd(), seri.getAciklama(), seri.getImagePath()));
//        }
//        return seriItemList;
//    }
//
//    @RequestMapping(value = "/sendOrder", method = RequestMethod.POST, headers = "Accept=application/json")
//    @ResponseBody
//    public HttpStatus siparisGonder(@RequestBody Order order) {
//
//        Kullanici kullanici = kullaniciService.getKullaniciById(order.getUser_id().longValue());
//        Firma firma = firmaService.getSingleById(order.getCompany_id().longValue());
//        List<ProductItem> productItemList = order.getProductItemList();
//
//        Date siparisZamani = DateUtils.getDate(order.getOrderDate(), "dd/mm/yyyy");
//
//        // session info set edliyr.
//        SessionInfo sessionInfo = new SessionInfo();
//        sessionInfo.setSirket(new Sirket(Sirket.ENUM.VARSAYILAN_SIRKET));
//        sessionInfo.setKullanici(kullanici);
//
//        // siparis bilgileri set ediliyor.
//        Siparis siparis = new Siparis();
//        siparis.setAciklama(order.getAciklama());
//        siparis.setFirma(firma);
//        siparis.setSiparisZamani(siparisZamani);
//
//        // siparis detay listesi aliniyor.
//        List<SiparisDetay> siparisDetayList = new ArrayList<>();
//        for (ProductItem pi : productItemList) {
//            // urun kaydi cekiliyor.
//            Urun urun = urunService.getUrunById(pi.getId().longValue());
//            SiparisDetay siparisDetay = new SiparisDetay();
//            siparisDetay.setUrun(urun);
//            siparisDetay.setSiparisAdedi(pi.getQuantity());
//            siparisDetayList = firmaService.firmaSiparisDetayEkleme(siparisDetayList, siparisDetay, sessionInfo);
//        }
//
//        // siparisler g�nderiliyor.
//        firmaService.firmaSiparisGonder(siparis, siparisDetayList, sessionInfo);
//
//        return HttpStatus.OK;
//    }
//

}
