package com.restServices.common.controller;

import com.restServices.common.model.RestKitap;
import com.ronin.common.service.IKitapService;
import com.ronin.common.service.IKullaniciService;
import com.ronin.model.Kitap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by fcabi on 06.01.2015.
 */

@RestController
@RequestMapping("/kitapController")
public class KitapController {

    @Autowired
    IKitapService kitapService;

    @RequestMapping(value = "/kitaplar", method = RequestMethod.GET, produces = "application/json")
    List<RestKitap>  getAllKitapList() {
        RestKitap restKitap = new RestKitap("sava? ve bar??", "tolstoy");
        List<RestKitap> restKitapList= new ArrayList<>();
        restKitapList.add(restKitap);

        return restKitapList;
    }

}
