package com.restServices.common.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


/**
 * Created by fcabi on 06.01.2015.
 */

@RestController
@RequestMapping("/barkod")
public class BarkodController {
    

    @RequestMapping(value = "/{barkodNo}", method = RequestMethod.GET)
    public ModelAndView getEnvanterByBarkodNo(@PathVariable String barkodNo) {
        Object envanter = null;

        ModelAndView model = new ModelAndView("envanterSorgula");
        model.addObject("envanter", envanter);

        return model;
    }

}
